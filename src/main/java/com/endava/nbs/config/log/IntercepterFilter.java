package com.endava.nbs.config.log;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Enumeration;

@Component
public class IntercepterFilter implements Filter {

    private DataSource dataSource;

    @Autowired
    public IntercepterFilter(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private void logIntoDB(String requestBody, String responseBody, HttpServletRequest requestWrapper, HttpServletResponse responseWrapper) throws IOException {
        HttpHeaders requestHeaders = new HttpHeaders();
        Enumeration headerNames = requestWrapper.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = (String) headerNames.nextElement();
            requestHeaders.add(headerName, requestWrapper.getHeader(headerName));
        }

        try (Connection connection = dataSource.getConnection();
             PreparedStatement log = connection.prepareStatement("" +
                     "INSERT INTO watchdog(ip,inPayload,outPayload,uri,method,inHeaders,responseCode) " +
                     "VALUES (?,?,?,?,?,?,?)")
        ) {
            log.setString(1, requestWrapper.getRemoteAddr());
            log.setString(2, requestBody);
            log.setString(3, responseBody);
            log.setString(4, requestWrapper.getRequestURI());
            log.setString(5, requestWrapper.getMethod());
            log.setString(6, requestHeaders.toString());
            log.setInt(7, responseWrapper.getStatus());
            log.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {
        ByteArrayPrinter pw = new ByteArrayPrinter();

        HttpServletRequest requestWrapper = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpServletResponse responseWrapper = new HttpServletResponseWrapper(response) {
            @Override
            public void setContentType(final String type) {
                super.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
            }

            @Override
            public PrintWriter getWriter() {
                return pw.getWriter();
            }

            @Override
            public ServletOutputStream getOutputStream() throws IOException {

                return pw.getStream();
            }
        };

        servletRequest = (new RequestWrapper((HttpServletRequest) servletRequest));
        String requestBody = ((RequestWrapper) servletRequest).getBody();

        chain.doFilter(servletRequest, responseWrapper);
        byte[] bytes = pw.toByteArray();

        String respBody = new String(bytes);

        logIntoDB(requestBody, respBody, requestWrapper, responseWrapper);
        response.getOutputStream().write(bytes);
    }
}