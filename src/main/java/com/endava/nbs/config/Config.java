package com.endava.nbs.config;

import oracle.jdbc.datasource.OracleDataSource;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@EnableJpaRepositories(
        basePackageClasses = {com.endava.nbs.domain.dao.PassengerRepository.class}
)
@EntityScan("com.endava.nbs.domain.entity")
@EnableTransactionManagement
public class Config {

    @Bean
    DataSource dataSource() throws SQLException {
        OracleDataSource dataSource = new oracle.jdbc.pool.OracleDataSource();
        dataSource.setUser("nbsuser");
        dataSource.setPassword("nbspassword");
        dataSource.setURL("jdbc:oracle:thin:@//localhost:1521/orclpdb");
        dataSource.setImplicitCachingEnabled(true);
        return dataSource;
    }

}


