package com.endava.nbs.domain.exception;


import org.springframework.web.client.HttpStatusCodeException;

import static org.springframework.http.HttpStatus.NOT_FOUND;

public class ResourceNotFoundException extends HttpStatusCodeException {

    public static ResourceNotFoundException ofId(final Object id) {
        return new ResourceNotFoundException("Resource with id: " + id + " not found");
    }

    public static ResourceNotFoundException ofNull() {
        return new ResourceNotFoundException("Can not find parent values!");
    }


    private ResourceNotFoundException(final String message) {
        super(NOT_FOUND, message);
    }
}
