package com.endava.nbs.domain.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;


public class BadRequestException extends HttpStatusCodeException {

    public BadRequestException(final HttpStatus httpStatus, final String message) {
        super(httpStatus, message);
    }
}
