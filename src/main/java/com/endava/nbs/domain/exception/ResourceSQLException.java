package com.endava.nbs.domain.exception;

import org.springframework.web.client.HttpStatusCodeException;

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

public class ResourceSQLException extends HttpStatusCodeException {
    public static ResourceSQLException ofId(final Object id) {
        return new ResourceSQLException("Error: " + id);
    }

    public ResourceSQLException(final String message) {
        super(NOT_ACCEPTABLE, message);
    }
}
