package com.endava.nbs.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;


@Builder
@ApiModel(description = "All details about the Route. ")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@EqualsAndHashCode
@Component
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RouteDto {
    @ApiModelProperty(notes = "Database generated route ID")
    private String idRoute;

    @ApiModelProperty(notes = "Source location ID")
    private String locationSrc;

    @ApiModelProperty(notes = "Destination location ID")
    private String locationDest;

    @ApiModelProperty(notes = "Race departure time")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime timeDeparture;

    @ApiModelProperty(notes = "Race arrival time")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime timeArrival;

    @ApiModelProperty(notes = "Database  buss's id")
    private Long idBus;

    @ApiModelProperty(notes = "Number of sold business class seats")
    private Long soldBussines;

    @ApiModelProperty(notes = "Number of sold economy class seats")
    private Long soldEconomy;

    @Override
    public String toString() {
        return "DtoRoute{" +
                "idRoute='" + idRoute + '\'' +
                ", locationSrc='" + locationSrc + '\'' +
                ", locationDest='" + locationDest + '\'' +
                ", timeDeparture=" + timeDeparture +
                ", timeArrival=" + timeArrival +
                ", idBus=" + idBus +
                ", soldBussines=" + soldBussines +
                ", soldEconomy=" + soldEconomy +
                '}';
    }
}
