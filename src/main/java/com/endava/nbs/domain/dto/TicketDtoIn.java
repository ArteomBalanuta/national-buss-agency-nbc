package com.endava.nbs.domain.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class TicketDtoIn {

    @NotNull
    private String idPassport;

    @NotNull
    private String idRoute;

    @ApiModelProperty(allowableValues = "BUS, ECO")
    private String classType;

    @ApiModelProperty(allowableValues = "LRG, SML")
    private String luggageType;
}
