package com.endava.nbs.domain.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@ApiModel(description = "All details about the Bus. ")
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Component
public class BusDto {
    @ApiModelProperty(notes = "Database generated Bus ID")
    private Integer idBus;
    @ApiModelProperty(notes = "Factory number of the Bus")
    private String factoryNR;
    @ApiModelProperty(notes = "Type of Bus - MINIVAN, STANDARD or DOUBLE-DECKER")
    private String typeBus;
    @ApiModelProperty(notes = "Capacity of econom seats in the bus")
    private byte capacityEconom;
    @ApiModelProperty(notes = "Capacity of business seats in the bus")
    private byte capacityBusiness;
    @ApiModelProperty(notes = "Date of last periodic inspection")
    private LocalDateTime timeInspection;

}