package com.endava.nbs.domain.dto;

import lombok.Data;

import java.util.List;

@Data
public class TicketsPageDto {

    private Long count;

    private Integer offset;

    private Integer limit;

    private Long total;

    private List<TicketDtoOut> items;
}
