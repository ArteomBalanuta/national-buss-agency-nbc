package com.endava.nbs.domain.dto;

import lombok.Data;

@Data
public class TicketDtoOut {
    private Integer idTicket;

    private String idPassport;

    private String idRoute;

    private String classType;

    private Double price;

    private Integer seatNr;

    private String luggageType;
}
