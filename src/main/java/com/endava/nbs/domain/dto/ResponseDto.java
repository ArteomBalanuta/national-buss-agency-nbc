package com.endava.nbs.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@Setter
@Getter
public class ResponseDto {
    private String msg;


    public ResponseDto(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "DtoResponse{" +
                "msg='" + msg + '\'' +
                '}';
    }
}
