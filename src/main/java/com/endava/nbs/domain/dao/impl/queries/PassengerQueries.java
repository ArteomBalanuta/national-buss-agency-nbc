package com.endava.nbs.domain.dao.impl.queries;

public abstract class PassengerQueries {
    public static String SELECT_Passenger_BY_ID = "SELECT idPassport,firstName,lastName,birthDate FROM Passengers WHERE idPassport = ?";
    public static String INSERT_Passenger = "INSERT INTO Passengers(idPassport,firstName,lastName,birthDate) VALUES (?,?,?,TO_DATE(?,'yyyy-MM-dd'))";
    public static String DELETE_Passenger_BY_ID = "DELETE FROM Passengers WHERE idPassport= ? ";
    public static String UPDATE_Passenger_BY_ID = "UPDATE Passengers SET firstName=?,lastName=?, birthDate=TO_DATE(?,'yyyy-MM-dd') WHERE idPassport = ? ";
    public static String SELECT_ALL_Passengers = "SELECT * FROM Passengers";


}
