package com.endava.nbs.domain.dao;

import com.endava.nbs.domain.entity.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketsRepository extends PagingAndSortingRepository<Ticket, Integer> {
    @Override
    Page<Ticket> findAll(Pageable pageable);
}
