package com.endava.nbs.domain.dao;

import com.endava.nbs.domain.entity.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PassengerRepository extends JpaRepository<Passenger, String> {
}
