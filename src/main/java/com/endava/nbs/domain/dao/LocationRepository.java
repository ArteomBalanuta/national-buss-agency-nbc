package com.endava.nbs.domain.dao;

import com.endava.nbs.domain.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, String> {

    default Location insertLocation(Location location) {
        return saveAndFlush(location);
    }

    default void deleteLocationById(String id) {
        deleteById(id);
        flush();
    }

    default Optional<Location> getLocationById(String id) {
        return findById(id);
    }

    default List<Location> getAllLocations() {
        return findAll();
    }
}
