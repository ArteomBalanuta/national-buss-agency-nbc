package com.endava.nbs.domain.dao;

import com.endava.nbs.domain.entity.Ticket;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Integer> {

    default Ticket insertTicket(Ticket ticket) {
        return saveAndFlush(ticket);
    }

    default void deleteTicketById(Integer id) {
        deleteById(id);
        flush();
    }

    default Optional<Ticket> getTicketById(Integer id) {
        return findById(id);
    }

    default List<Ticket> getAllTickets() {
        return findAll();
    }
}
