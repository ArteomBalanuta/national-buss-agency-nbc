package com.endava.nbs.domain.dao;

import com.endava.nbs.domain.entity.Bus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


public interface BusRepository extends JpaRepository<Bus , Integer>{
    default Optional<Bus> getBusById(Integer idBus){
        return getBusById(idBus);
    };

}
