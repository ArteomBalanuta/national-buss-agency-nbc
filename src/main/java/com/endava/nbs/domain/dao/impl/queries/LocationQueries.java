package com.endava.nbs.domain.dao.impl.queries;

public abstract class LocationQueries {
    public static final String SELECT_LOCATION_BY_ID = "SELECT idLocation, cityName FROM Locations WHERE idLocation = ?";
    public static final String INSERT_INTO_LOCATIONS = "INSERT INTO Locations VALUES (?, ?)";
    public static final String DELETE_LOCATION_BY_ID = "DELETE FROM Locations WHERE idLocation = ?";
    public static final String UPDATE_LOCATION_BY_ID = "UPDATE Locations SET cityName=? WHERE idLocation = ?";
    public static final String SELECT_ALL_FROM_LOCATIONS = "SELECT * FROM Locations";
}
