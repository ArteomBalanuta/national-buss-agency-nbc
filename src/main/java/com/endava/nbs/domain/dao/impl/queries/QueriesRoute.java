package com.endava.nbs.domain.dao.impl.queries;

public abstract class QueriesRoute {
    public static final String SELECT_ROUTE_BY_ID = "SELECT idRoute,srcLocation,destLocation,depTime,arrivalTime,idBus,soldBusiness,soldEconomy FROM Routes WHERE idRoute = ?";
    public static final String INSERT_ROUTE = "INSERT INTO Routes(idRoute,srcLocation,destLocation,depTime,arrivalTime,idBus,soldBusiness,soldEconomy) VALUES (?,?,?,TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),?,?,?)";
    public static final String DELETE_ROUTE_BY_ID = "DELETE FROM Routes WHERE idRoute = ? ";
    public static final String UPDATE_ROUTE_BY_ID = "UPDATE Routes SET srcLocation=?,destLocation=?,depTime=TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),arrivalTime=TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),idBus=?,soldBusiness=?,soldEconomy=? WHERE idRoute = ? ";
    public static final String SELECT_ALL_ROUTES = "SELECT idRoute,srcLocation,destLocation,depTime,arrivalTime,idBus,soldBusiness,soldEconomy FROM Routes";
    public static final String DELETE_ALL_ROUTES = "DELETE FROM Routes";
}
