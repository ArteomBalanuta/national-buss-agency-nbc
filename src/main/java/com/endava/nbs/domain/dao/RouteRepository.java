package com.endava.nbs.domain.dao;


import com.endava.nbs.domain.entity.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RouteRepository extends JpaRepository<Route, String> {

    default String insertRoute(Route route) throws RuntimeException {
        return saveAndFlush(route).getIdRoute();
    }

    default void deleteRouteById(String id) throws Exception {
        deleteById(id);
        flush();
    }

    default Optional<Route> getRouteById(String id) {
        return findById(id);
    }

    default void updateRouteById(String id, Route newRoute) throws RuntimeException {
        newRoute.setIdRoute(id);
        saveAndFlush(newRoute);
    }

    default List<Route> getAllRoutes() {
        return findAll();
    }
}
