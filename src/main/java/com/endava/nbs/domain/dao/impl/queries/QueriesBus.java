package com.endava.nbs.domain.dao.impl.queries;

public abstract class QueriesBus {
    public static final String SELECT_BUS_BY_ID = "SELECT idBus,factoryNr,typeBus,capacityEconom,capacityBusiness,timeInspection FROM Buses WHERE idBus = ?";
    public static final String INSERT_BUS = "INSERT INTO Buses(factoryNr,typeBus,capacityEconom,capacityBusiness,timeInspection) VALUES (?,?,?,?,TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'))";
    public static final String DELETE_BUS_BY_ID = "DELETE FROM Buses WHERE idBus = ? ";
    public static final String UPDATE_BUS_BY_ID = "UPDATE Buses SET factoryNr=?,typeBus=?,capacityEconom=?,capacityBusiness=?,timeInspection=TO_DATE(?,'YYYY-MM-DD hh24-mi-ss') WHERE idBus = ? ";
    public static final String SELECT_ALL_BUSES = "SELECT idBus,factoryNr,typeBus,capacityEconom,capacityBusiness,timeInspection FROM Buses";

}
