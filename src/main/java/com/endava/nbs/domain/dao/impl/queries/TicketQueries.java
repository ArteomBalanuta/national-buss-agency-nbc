package com.endava.nbs.domain.dao.impl.queries;

public abstract class TicketQueries {
    public static final String SELECT_TICKET_BY_ID = "SELECT idTicket, idPassport, idRoute, class, price, seatNr, luggageType FROM Tickets WHERE idTicket = ?";
    public static final String INSERT_TICKET = "INSERT INTO Tickets (idPassport, idRoute, class, price, seatNr, luggageType) VALUES (?,?,?,?,?,?)";
    public static final String DELETE_TICKET_BY_ID = "DELETE FROM Tickets WHERE idTicket = ?";
    public static final String UPDATE_TICKET_BY_ID = "UPDATE Tickets SET idPassport = ?, idRoute = ?, class = ?, price = ?, seatNr = ?, luggageType = ? WHERE idTicket = ?";
    public static final String SELECT_ALL_FROM_TICKETS = "SELECT idTicket, idPassport, idRoute, class, price, seatNr, luggageType FROM Tickets";
}
