package com.endava.nbs.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;
@AllArgsConstructor
@Builder
@Data
@NoArgsConstructor
@Entity
@Table(name = "PASSENGERS")
public class Passenger {
    @Id
    @Column(name = "IDPASSPORT", length = 13)
    private String idPassport;
    @Column(name = "FIRSTNAME", length = 30)
    private String firstName;
    @Column(name = "LASTNAME", length = 30)
    private String lastName;
    @Column(name = "BIRTHDATE", nullable = false)
    private LocalDate birthDate;
}
