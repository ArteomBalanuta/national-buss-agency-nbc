package com.endava.nbs.domain.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@EqualsAndHashCode

//@Proxy(lazy = false)
@Entity
@Table(name = "Routes")
public class Route {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(name = "IDROUTE", updatable = false, nullable = false)
    private String idRoute;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SRCLOCATION", referencedColumnName = "idLocation", nullable = false)
    private Location locationSrc;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DESTLOCATION", referencedColumnName = "idLocation", nullable = false)
    private Location locationDest;

    @Column(name = "DEPTIME")
    private LocalDateTime timeDeparture;

    @Column(name = "ARRIVALTIME")
    private LocalDateTime timeArrival;

    @Column(name = "IDBUS", nullable = false)
    private Long idBus;

    @Column(name = "SOLDBUSINESS")
    private Long soldBussines;

    @Column(name = "SOLDECONOMY")
    private Long soldEconomy;

    public Route(Location locationSrc, Location locationDest, LocalDateTime timeDeparture, LocalDateTime timeArrival, Long idBus, Long soldBussines, Long soldEconomy) {
        this.locationSrc = locationSrc;
        this.locationDest = locationDest;
        this.timeDeparture = timeDeparture;
        this.timeArrival = timeArrival;
        this.idBus = idBus;
        this.soldBussines = soldBussines;
        this.soldEconomy = soldEconomy;
    }
}