package com.endava.nbs.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "Tickets")
public class Ticket {

    @Id
    @Column(name = "IDTICKET")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ticketsSequence")
    @SequenceGenerator(name = "ticketsSequence", sequenceName = "tickets_seq", allocationSize = 1)
    private Integer idTicket;

    @ManyToOne
    @JoinColumn(name = "IDPASSPORT")
    private Passenger passenger;

    @ManyToOne
    @JoinColumn(name = "IDROUTE")
    private Route route;

    @Column(name = "CLASS")
    private String classType;

    @Column(name = "PRICE")
    private Double price;

    @Column(name = "SEATNR")
    private Integer seatNr;

    @Column(name = "LUGGAGETYPE")
    private String luggageType;
}

