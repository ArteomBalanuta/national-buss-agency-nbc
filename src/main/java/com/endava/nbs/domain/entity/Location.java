package com.endava.nbs.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "Locations")
public class Location {

    @Id
    @Column(name = "IDLOCATION")
    String idLocation;

    @Column(name = "CITYNAME")
    String cityName;


    public Location(String idLocation) {
        this.idLocation = idLocation;
    }
}
