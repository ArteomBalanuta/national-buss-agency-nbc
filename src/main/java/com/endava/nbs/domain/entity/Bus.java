package com.endava.nbs.domain.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@Entity
@Table(name = "Buses")
public class Bus {

    @Id
    @Column(name="IDBUS",length = 6, nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer idBus;

    @Column(name="FACTORYNR",nullable=false)
    private String factoryNR;

    @Column(name="TYPEBUS",nullable=false)
    private String typeBus;

    @Column(name="CAPACITYECONOM")
    private byte capacityEconom;

    @Column(name="CAPACITYBUSINESS")
    private byte capacityBusiness;

    @Column(name="TIMEINSPECTION",nullable=false)
    private LocalDateTime timeInspection;

}
