package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.BusRepository;
import com.endava.nbs.domain.entity.Bus;
import com.endava.nbs.domain.mapper.BusMapper;
import com.endava.nbs.domain.service.BusService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BusServiceImpl implements BusService {

    private final BusRepository busRepository;

    private final BusMapper busMapper;

    @Override
    public Optional<Bus> getBusById(Integer idBus){
        return busRepository.findById(idBus);
    }

    @Override
    public Bus deleteBusById(Integer idBus){
        Optional<Bus> bus = busRepository.findById(idBus);
        busRepository.deleteById(idBus);
        return  bus.orElseThrow(() -> new RuntimeException("no bus"));
    }

    @Override
    public Bus insertBus(Bus bus) throws Exception {
        Bus savedBus = busRepository.saveAndFlush(bus);
        return savedBus;
    }

    @Override
    public Bus updateBusById(Integer idBus, Bus bus) throws Exception {
        return busRepository.saveAndFlush(bus);
    }

    @Override
    public List<Bus> getAllBuses() throws Exception {
        return busRepository.findAll();
    }
}
