package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.BusRepository;
import com.endava.nbs.domain.dao.RouteRepository;
import com.endava.nbs.domain.dao.TicketRepository;
import com.endava.nbs.domain.dto.TicketDtoIn;
import com.endava.nbs.domain.dto.TicketDtoOut;
import com.endava.nbs.domain.entity.Bus;
import com.endava.nbs.domain.entity.Route;
import com.endava.nbs.domain.entity.Ticket;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.mapper.TicketMapper;
import com.endava.nbs.domain.service.TicketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TicketService {


    private static final double FIX_TICKET_COST = 100d;
    private static final double LRG_BAGGAGE_COST = 30d;
    private static final double BUS_CLASS_COST = 20d;

    private final BusRepository busRepository;
    private final TicketRepository ticketRepository;
    private final RouteRepository repositoryRoute;
    private final TicketMapper ticketMapper;


    public TicketDtoOut insertTicket(TicketDtoIn ticketDtoIn) {
        validateTicketInput(ticketDtoIn);
        Ticket ticket = ticketMapper.toEntity(ticketDtoIn);
        ticket.setPrice(calculatePrice(ticketDtoIn));
        ticket.setSeatNr(getSeatNr(ticketDtoIn));
        TicketDtoOut insertedTicket = ticketMapper.toDtoOut(ticketRepository.insertTicket(ticket));
        incrementRouteSoldTickets(ticketDtoIn);
        return insertedTicket;
    }

    public void deleteTicketById(Integer id) {
        getTicketById(id);
        ticketRepository.deleteTicketById(id);
    }

    public TicketDtoOut getTicketById(Integer id) {
        Ticket ticket = ticketRepository.getTicketById(id).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Ticket with id: " + id + " NOT FOUND"));
        return ticketMapper.toDtoOut(ticket);
    }

    public List<TicketDtoOut> getAllTickets() {
        return ticketRepository.getAllTickets().stream().map(ticketMapper::toDtoOut).collect(Collectors.toList());
    }

    public TicketDtoOut updateTicketById(Integer id, TicketDtoIn newTicketDtoIn) {
        validateTicketInput(newTicketDtoIn);

        Ticket newTicket = ticketMapper.toEntity(newTicketDtoIn);
        Ticket oldTicket = ticketRepository.getTicketById(id).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Ticket with id: " + id + " NOT FOUND"));
        int newSeatNr = newTicketDtoIn.getClassType().equals(oldTicket.getClassType()) ? oldTicket.getSeatNr() : getSeatNr(newTicketDtoIn);

        newTicket.setIdTicket(oldTicket.getIdTicket());
        newTicket.setRoute(oldTicket.getRoute());
        newTicket.setPrice(calculatePrice(newTicketDtoIn));
        newTicket.setSeatNr(newSeatNr);

        if (!newTicketDtoIn.getClassType().equals(oldTicket.getClassType())) {
            updateRouteSoldTickets(oldTicket.getRoute(), newTicketDtoIn.getClassType());
        }

        return ticketMapper.toDtoOut(ticketRepository.insertTicket(newTicket));
    }

    private double calculatePrice(TicketDtoIn ticketDtoIn) {
        double ticketPrice = FIX_TICKET_COST;

        ticketPrice += ticketDtoIn.getClassType().equals("BUS") ? BUS_CLASS_COST : 0;
        ticketPrice += ticketDtoIn.getLuggageType().equals("LRG") ? LRG_BAGGAGE_COST : 0;

        return ticketPrice;
    }

    private int getSeatNr(TicketDtoIn ticketDtoIn) {
        int seatNr = -1;

        Route route = getRouteForTicketDto(ticketDtoIn);
        Bus bus = busRepository.getOne(route.getIdBus().intValue());

        if (ticketDtoIn.getClassType().equals("BUS")) {
            if (route.getSoldBussines() >= bus.getCapacityBusiness()) {
                throw new BadRequestException(HttpStatus.BAD_REQUEST, "No Business Class tickets available on Route: " + ticketDtoIn.getIdRoute());
            }
            seatNr = route.getSoldBussines().intValue() + 1;
        }

        if (ticketDtoIn.getClassType().equals("ECO")) {
            if (route.getSoldEconomy() >= bus.getCapacityEconom()) {
                throw new BadRequestException(HttpStatus.BAD_REQUEST, "No Economy Class tickets available on Route: " + ticketDtoIn.getIdRoute());
            }
            seatNr = bus.getCapacityBusiness() + route.getSoldEconomy().intValue() + 1;
        }

        return seatNr;
    }

    private Route getRouteForTicketDto(TicketDtoIn ticketDtoIn) {

        return repositoryRoute.getRouteById(ticketDtoIn.getIdRoute()).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "No Route with id: " + ticketDtoIn.getIdRoute()));
    }

    private void incrementRouteSoldTickets(TicketDtoIn ticketDtoIn) {
        Route route = getRouteForTicketDto(ticketDtoIn);

        if (ticketDtoIn.getClassType().equals("BUS")) {
            route.setSoldBussines(route.getSoldBussines() + 1);
        }
        if (ticketDtoIn.getClassType().equals("ECO")) {
            route.setSoldEconomy(route.getSoldEconomy() + 1);
        }

        repositoryRoute.updateRouteById(ticketDtoIn.getIdRoute(), route);
    }

    private void updateRouteSoldTickets(Route route, String newTravelClass) {

        if (newTravelClass.equals("BUS")) {
            route.setSoldBussines(route.getSoldBussines() + 1);
            route.setSoldEconomy(route.getSoldEconomy() - 1);
        }
        if (newTravelClass.equals("ECO")) {
            route.setSoldEconomy(route.getSoldEconomy() + 1);
            route.setSoldBussines(route.getSoldBussines() - 1);
        }

        repositoryRoute.updateRouteById(route.getIdRoute(), route);
    }

    private void validateTicketInput(TicketDtoIn ticketDtoIn) {

        if (!(ticketDtoIn.getClassType().equals("BUS") || ticketDtoIn.getClassType().equals("ECO"))) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Incorrect 'classType'. Acceptable values: 'BUS', 'ECO");
        }
        if (!(ticketDtoIn.getLuggageType().equals("LRG") || ticketDtoIn.getLuggageType().equals("SML"))) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Incorrect 'luggageType'. Acceptable values: 'LRG', 'SML");
        }
    }
}
