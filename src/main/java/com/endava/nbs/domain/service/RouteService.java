package com.endava.nbs.domain.service;

import com.endava.nbs.domain.dto.RouteDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RouteService {

    RouteDto getRouteById(String id) throws Exception;

    RouteDto deleteRouteById(String id) throws Exception;

    RouteDto insertRoute(RouteDto route) throws Exception;

    RouteDto updateRouteById(String id, RouteDto route) throws Exception;

    List<RouteDto> getAllRoutes() throws Exception;

}
