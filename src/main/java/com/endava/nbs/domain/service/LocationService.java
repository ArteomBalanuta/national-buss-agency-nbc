package com.endava.nbs.domain.service;

import com.endava.nbs.domain.entity.Location;

import java.util.List;

public interface LocationService {

    Location getLocationById(String id);

    Location insertLocation(Location location);

    void deleteLocationById(String id);

    Location updateLocationById(String id, Location newLocation);

    List<Location> getAllLocations();

}
