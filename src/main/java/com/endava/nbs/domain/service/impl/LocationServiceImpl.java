package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.LocationRepository;
import com.endava.nbs.domain.entity.Location;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {

    private final LocationRepository locationRepository;

    public Location getLocationById(String id) {
        validateLocationId(id);

        return locationRepository.getLocationById(id).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Location with id: " + id + " NOT FOUND"));
    }

    public Location insertLocation(Location location) {
        validateLocationId(location.getIdLocation());

        if (locationRepository.getLocationById(location.getIdLocation()).isPresent()) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Location with id '" + location.getIdLocation() + "' ALREADY EXISTS");
        }
        return locationRepository.insertLocation(location);
    }

    public void deleteLocationById(String id) {
        validateLocationId(id);

        if (getLocationById(id) != null) {
            locationRepository.deleteLocationById(id);
        }
    }

    public Location updateLocationById(String id, Location newLocation) {
        validateLocationId(id);
        validateLocationId(newLocation.getIdLocation());

        Location oldLocation = locationRepository.getLocationById(id).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Location with id: '" + id + "' NOT FOUND"));
        Location updatedLocation = new Location();

        updatedLocation.setIdLocation(oldLocation.getIdLocation());
        updatedLocation.setCityName(newLocation.getCityName());

        return locationRepository.insertLocation(updatedLocation);
    }

    public List<Location> getAllLocations() {
        return locationRepository.getAllLocations();
    }

    private void validateLocationId(String id) {

        if (id == null) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Incorrect input Json");
        }

        if (id.length() != 3) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST,
                    "Location id should be 3 characters long. The inserted id has " + id.length() + " characters");
        }
    }
}