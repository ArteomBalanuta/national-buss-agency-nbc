package com.endava.nbs.domain.service;

import com.endava.nbs.domain.entity.Bus;

import java.util.List;
import java.util.Optional;



public interface BusService {

    Optional<Bus> getBusById(Integer idBus) throws Exception;

    Bus deleteBusById(Integer idBus) throws Exception;

    Bus insertBus(Bus bus) throws Exception;

    Bus updateBusById(Integer idBus, Bus bus) throws Exception;

    List<Bus> getAllBuses() throws Exception;

}
