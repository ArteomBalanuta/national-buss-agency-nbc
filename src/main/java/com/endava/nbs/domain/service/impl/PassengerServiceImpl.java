package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.PassengerRepository;
import com.endava.nbs.domain.entity.Passenger;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.service.PassengerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PassengerServiceImpl implements PassengerService {
    private final PassengerRepository passengerRepository;

    public Passenger getPassengerById(String id) {
        return passengerRepository.findById(id).orElseThrow(()
                -> new BadRequestException(HttpStatus.NOT_FOUND, "Location with id: " + id + " NOT FOUND"));
    }

    public void insertPassenger(Passenger passenger) {
        if (passengerRepository.findById(passenger.getIdPassport()).isPresent())
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Passenger with passport id: " + passenger.getIdPassport() + " already exists.");
        passengerRepository.save(passenger);
    }

    public void deleteById(String id) {
        passengerRepository.findById(id);
        passengerRepository.deleteById(id);
    }

    public Passenger updatePassengerById(String id, Passenger passenger) {
        if (!passengerRepository.existsById(id)) {
            throw new BadRequestException(HttpStatus.NOT_FOUND, "Passenger with id: " + id + " NOT FOUND");
        }
        return passengerRepository.save(passenger);
    }

    public List<Passenger> getAllPassengers() {
        return (List<Passenger>) passengerRepository.findAll();
    }
}

















