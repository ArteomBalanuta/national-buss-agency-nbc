package com.endava.nbs.domain.service;

import com.endava.nbs.domain.dto.TicketDtoIn;
import com.endava.nbs.domain.dto.TicketDtoOut;

import java.util.List;

public interface TicketService {

    TicketDtoOut insertTicket(TicketDtoIn ticketDtoIn);

    void deleteTicketById(Integer id);

    TicketDtoOut getTicketById(Integer id);

    TicketDtoOut updateTicketById(Integer id, TicketDtoIn newTicketDtoIn);

    List<TicketDtoOut> getAllTickets();
}
