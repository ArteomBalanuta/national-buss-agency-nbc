package com.endava.nbs.domain.service;

import com.endava.nbs.domain.entity.Passenger;

import java.util.List;

public interface PassengerService {
    Passenger getPassengerById(String id);

    void insertPassenger(Passenger passenger);

    void deleteById(String id);

    Passenger updatePassengerById(String id, Passenger passenger);

    public List<Passenger> getAllPassengers();
}
