package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.TicketsRepository;
import com.endava.nbs.domain.dto.TicketsPageDto;
import com.endava.nbs.domain.entity.Ticket;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.mapper.impl.TicketsPageMapperImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TicketsService {

    private final TicketsRepository ticketsRepository;
    private final TicketsPageMapperImpl ticketsPageMapper;

    public TicketsPageDto getTicketsPage(int offset, int limit) {

        if (offset < 0) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Offset must not be less than 0!");
        }
        if (limit < 1) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST, "Limit must not be less than 1!");
        }

        Pageable ticketsPageable = new OffsetLimitPageable(offset, limit, Sort.by("idTicket").descending());
        Page<Ticket> ticketPage = ticketsRepository.findAll(ticketsPageable);

        return ticketsPageMapper.toDto(ticketPage, offset, limit);
    }

    private class OffsetLimitPageable implements Pageable {
        private int limit;
        private int offset;
        private Sort sort;

        public OffsetLimitPageable(int offset, int limit, Sort sort) {
            this.offset = offset;
            this.limit = limit;
            this.sort = sort;
        }

        @Override
        public int getPageNumber() {
            return offset / limit;
        }

        @Override
        public int getPageSize() {
            return limit;
        }

        @Override
        public long getOffset() {
            return offset;
        }

        @Override
        public Sort getSort() {
            return sort;
        }

        @Override
        public Pageable next() {
            return new OffsetLimitPageable(getPageSize(), (int) (getOffset() + getPageSize()), sort);
        }

        public Pageable previous() {
            return hasPrevious() ?
                    new OffsetLimitPageable(getPageSize(), (int) (getOffset() - getPageSize()), sort) : this;
        }

        @Override
        public Pageable previousOrFirst() {
            return hasPrevious() ? previous() : first();
        }

        @Override
        public Pageable first() {
            return new OffsetLimitPageable(0, getPageSize(), sort);
        }

        @Override
        public boolean hasPrevious() {
            return offset > limit;
        }
    }
}
