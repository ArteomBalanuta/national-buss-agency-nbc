package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.RouteRepository;
import com.endava.nbs.domain.dto.RouteDto;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.mapper.RouteMapper;
import com.endava.nbs.domain.service.RouteService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Setter
@Getter
@Service
public class RouteServiceImpl implements RouteService {

    private final RouteRepository routeRepository;
    private final RouteMapper routeMapper;

    public RouteServiceImpl(final RouteRepository routeRepository, final RouteMapper routeMapper) {
        this.routeRepository = routeRepository;
        this.routeMapper = routeMapper;
    }

    public RouteDto getRouteById(final String id) {
        return routeMapper.toDto(routeRepository.getRouteById(id).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Route with id: " + id + " Not found!")));
    }

    public RouteDto deleteRouteById(final String id) throws Exception {
        RouteDto routeDto = routeMapper.toDto(routeRepository.getRouteById(id).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Route with id: " + id + " Not found!")));
        routeRepository.deleteRouteById(id);
        return routeDto;
    }

    //Returns id of inserted Object
    public RouteDto insertRoute(final RouteDto newRouteDto) {
        return RouteDto.builder()
                .idRoute(routeRepository.insertRoute(routeMapper.toEntity(newRouteDto)))
                .build();
    }

    public RouteDto updateRouteById(final String id, final RouteDto newRouteDto) {
        RouteDto routeDto = routeMapper.toDto(routeRepository.getRouteById(id).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Route with id: " + id + " Not found!")));
        routeRepository.updateRouteById(id, routeMapper.toEntity(newRouteDto));
        return routeDto;
    }

    public List<RouteDto> getAllRoutes() {
        return routeRepository.getAllRoutes()
                .stream()
                .map(routeMapper::toDto)
                .collect(Collectors.toList());
    }
}
