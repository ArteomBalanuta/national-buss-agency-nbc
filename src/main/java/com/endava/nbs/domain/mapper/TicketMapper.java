package com.endava.nbs.domain.mapper;

import com.endava.nbs.domain.dto.TicketDtoIn;
import com.endava.nbs.domain.dto.TicketDtoOut;
import com.endava.nbs.domain.entity.Ticket;

public interface TicketMapper {
    TicketDtoOut toDtoOut(Ticket ticket);

    Ticket toEntity(TicketDtoIn ticketDtoIn);
}
