package com.endava.nbs.domain.mapper.impl;

import com.endava.nbs.domain.dto.TicketsPageDto;
import com.endava.nbs.domain.entity.Ticket;
import com.endava.nbs.domain.mapper.TicketMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class TicketsPageMapperImpl {

    private final TicketMapper ticketMapper;

    public TicketsPageDto toDto(Page<Ticket> ticketPage, Integer offset, Integer limit) {
        if (ticketPage == null) {
            return null;
        }
        TicketsPageDto ticketsPageDto = new TicketsPageDto();

        ticketsPageDto.setCount(ticketPage.getTotalElements() - offset);
        ticketsPageDto.setOffset(offset);
        ticketsPageDto.setLimit(limit);
        ticketsPageDto.setTotal(ticketPage.getTotalElements());
        ticketsPageDto.setItems(ticketPage.getContent().stream().map(ticketMapper::toDtoOut).collect(Collectors.toList()));

        return ticketsPageDto;
    }
}
