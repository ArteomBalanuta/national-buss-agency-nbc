package com.endava.nbs.domain.mapper.impl;

import com.endava.nbs.domain.dao.PassengerRepository;
import com.endava.nbs.domain.dao.RouteRepository;
import com.endava.nbs.domain.dto.TicketDtoIn;
import com.endava.nbs.domain.dto.TicketDtoOut;
import com.endava.nbs.domain.entity.Ticket;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.mapper.TicketMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TicketMapperImpl implements TicketMapper {

    private final PassengerRepository passengerRepository;
    private final RouteRepository routeRepository;

    public TicketDtoOut toDtoOut(Ticket ticket) {
        if (ticket == null) {
            return null;
        }
        TicketDtoOut ticketDtoOut = new TicketDtoOut();
        ticketDtoOut.setIdTicket(ticket.getIdTicket());
        ticketDtoOut.setIdPassport(ticket.getPassenger().getIdPassport());
        ticketDtoOut.setIdRoute(ticket.getRoute().getIdRoute());
        ticketDtoOut.setClassType(ticket.getClassType());
        ticketDtoOut.setLuggageType(ticket.getLuggageType());
        ticketDtoOut.setPrice(ticket.getPrice());
        ticketDtoOut.setSeatNr(ticket.getSeatNr());
        return ticketDtoOut;
    }

    public Ticket toEntity(TicketDtoIn ticketDtoIn) {
        if (ticketDtoIn == null) {
            return null;
        }
        Ticket ticket = new Ticket();
        ticket.setPassenger(passengerRepository.findById(ticketDtoIn.getIdPassport()).
                orElseThrow(() -> new BadRequestException(HttpStatus.NOT_FOUND, "Passenger with ID: " + ticketDtoIn.getIdPassport() + " NOT REGISTERED")));
        ticket.setRoute(routeRepository.getRouteById(ticketDtoIn.getIdRoute()).
                orElseThrow(() -> new BadRequestException(HttpStatus.NOT_FOUND, "Route with ID: " + ticketDtoIn.getIdRoute() + " NOT FOUND")));
        ticket.setClassType(ticketDtoIn.getClassType());
        ticket.setLuggageType(ticketDtoIn.getLuggageType());
        return ticket;
    }
}
