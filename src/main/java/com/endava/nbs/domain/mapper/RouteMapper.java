package com.endava.nbs.domain.mapper;

import com.endava.nbs.domain.dto.RouteDto;
import com.endava.nbs.domain.entity.Route;

public interface RouteMapper {
    RouteDto toDto(Route entity);

    Route toEntity(RouteDto dto);
}
