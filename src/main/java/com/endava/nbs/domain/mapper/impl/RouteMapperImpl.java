package com.endava.nbs.domain.mapper.impl;

import com.endava.nbs.domain.dao.LocationRepository;
import com.endava.nbs.domain.dto.RouteDto;
import com.endava.nbs.domain.entity.Route;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.mapper.RouteMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class RouteMapperImpl implements RouteMapper {

    private LocationRepository locationRepository;

    public RouteMapperImpl(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public RouteDto toDto(Route entity) {
        return new RouteDto(
                entity.getIdRoute(),
                entity.getLocationSrc().getIdLocation(),
                entity.getLocationDest().getIdLocation(),
                entity.getTimeDeparture(),
                entity.getTimeArrival(),
                entity.getIdBus(),
                entity.getSoldBussines(),
                entity.getSoldEconomy());
    }

    public Route toEntity(RouteDto dto) {
        if (!locationRepository.getLocationById(dto.getLocationSrc()).isPresent() ||
                !locationRepository.getLocationById(dto.getLocationDest()).isPresent()) {
            throw new BadRequestException(HttpStatus.NOT_FOUND, "Location not found!");
        }
        return new Route(
                locationRepository.getLocationById(dto.getLocationSrc()).get(),
                locationRepository.getLocationById(dto.getLocationDest()).get(),
                dto.getTimeDeparture(),
                dto.getTimeArrival(),
                dto.getIdBus(),
                dto.getSoldBussines(),
                dto.getSoldEconomy()
        );
    }
}
