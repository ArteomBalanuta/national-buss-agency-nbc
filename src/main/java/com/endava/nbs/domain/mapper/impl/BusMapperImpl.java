package com.endava.nbs.domain.mapper.impl;

import com.endava.nbs.domain.dto.BusDto;
import com.endava.nbs.domain.entity.Bus;
import com.endava.nbs.domain.mapper.BusMapper;
import org.springframework.stereotype.Component;

@Component
public class BusMapperImpl implements BusMapper {


    public BusDto toDto(Bus entity) {
        return new BusDto(
                entity.getIdBus(),
                entity.getFactoryNR(),
                entity.getTypeBus(),
                entity.getCapacityEconom(),
                entity.getCapacityBusiness(),
                entity.getTimeInspection()
        );
    }

    @Override
    public Bus toEntity(BusDto dto) {
        return new Bus(
                dto.getIdBus(),
                dto.getFactoryNR(),
                dto.getTypeBus(),
                dto.getCapacityEconom(),
                dto.getCapacityBusiness(),
                dto.getTimeInspection()
        );
    }
}
