package com.endava.nbs.domain.mapper;

import com.endava.nbs.domain.dto.BusDto;
import com.endava.nbs.domain.entity.Bus;

public interface BusMapper {
    BusDto toDto(Bus entity);

    Bus toEntity(BusDto dto);
}
