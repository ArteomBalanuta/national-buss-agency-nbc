package com.endava.nbs.web;

import com.endava.nbs.domain.dto.ResponseDto;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = HttpStatusCodeException.class)
    public ResponseEntity<Object> handleResourceNotFound(final HttpStatusCodeException ex, final WebRequest request) {
        return handleExceptionInternal(ex, apiMessage(ex), new HttpHeaders(), ex.getStatusCode(), request);
    }

    private ResponseDto apiMessage(final HttpStatusCodeException ex) {
        final String message = ex.getMessage() == null ? ex.getClass().getSimpleName() : ex.getMessage();
        return new ResponseDto(message);
    }
}
