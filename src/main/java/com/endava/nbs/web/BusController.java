package com.endava.nbs.web;

import com.endava.nbs.domain.entity.Bus;
import com.endava.nbs.domain.service.BusService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping("/bus")
public class BusController {


    private final BusService busService;

    public BusController(BusService busService) {
        this.busService = busService;
    }

    @GetMapping("/{idBus}")
    public ResponseEntity<Bus> getById(@PathVariable("idBus") final Integer idBus) throws Exception {
        return new ResponseEntity(busService.getBusById(idBus), HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<Bus>> getAllBuses() throws Exception {
        return new ResponseEntity(busService.getAllBuses(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<Bus> insertBus(@RequestBody final Bus busDto) throws Exception {
        return new ResponseEntity(busService.insertBus(busDto), HttpStatus.OK);
    }

    @DeleteMapping("/{idBus}")
    public ResponseEntity<Bus> deleteBusById(@PathVariable("idBus") final Integer idBus) throws Exception {
        return new ResponseEntity(busService.deleteBusById(idBus), HttpStatus.OK);
    }

    @PutMapping("/{idBus}")
    public ResponseEntity<Bus> updateBusById(@PathVariable("idBus") final Integer idBus, @RequestBody final Bus busDto) throws Exception {
        return new ResponseEntity(busService.updateBusById(idBus, busDto), HttpStatus.OK);
    }

}
