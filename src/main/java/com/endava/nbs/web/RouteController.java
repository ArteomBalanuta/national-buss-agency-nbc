package com.endava.nbs.web;


import com.endava.nbs.domain.dto.ResponseDto;
import com.endava.nbs.domain.dto.RouteDto;
import com.endava.nbs.domain.service.RouteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(value = "Route Management System", description = "Operations pertaining to routes in Route Management System")
@RestController
@RequestMapping("/route")
public class RouteController {

    private final
    RouteService serviceRoute;

    public RouteController(RouteService serviceRoute) {
        this.serviceRoute = serviceRoute;
    }

    @ApiOperation(value = "Get a route by Id", response = ResponseDto.class)
    @GetMapping("/{id}")
    public ResponseEntity<RouteDto> getById(@PathVariable("id") final String id) throws Exception {
        return new ResponseEntity<>(serviceRoute.getRouteById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Get all routes", response = ResponseDto.class)
    @GetMapping
    public ResponseEntity<RouteDto> getAllRoutes() throws Exception {
        return new ResponseEntity(serviceRoute.getAllRoutes(), HttpStatus.OK);
    }

    @ApiOperation(value = "Add a route", response = ResponseDto.class)
    @PostMapping
    public ResponseEntity<RouteDto> insertIntoRoutes(@RequestBody @Valid final RouteDto routeDto) throws Exception {
        return new ResponseEntity<>(serviceRoute.insertRoute(routeDto), HttpStatus.OK);
    }

    @ApiOperation(value = "delete a route", response = ResponseDto.class)
    @DeleteMapping("/{id}")
    public ResponseEntity<RouteDto> deleteRouteById(@PathVariable("id") final String id) throws Exception {
        return new ResponseEntity<>(serviceRoute.deleteRouteById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "update a route and returns updated one", response = ResponseDto.class)
    @PutMapping("/{id}")
    public ResponseEntity<RouteDto> updateRouteById(@PathVariable("id") final String id, @RequestBody @Valid final RouteDto routeDto) throws Exception {
        return new ResponseEntity(serviceRoute.updateRouteById(id, routeDto), HttpStatus.OK);
    }
}
