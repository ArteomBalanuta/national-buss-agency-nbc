package com.endava.nbs.web;

import com.endava.nbs.domain.entity.Location;
import com.endava.nbs.domain.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/location")
public class LocationController {

    private final LocationService locationServiceImpl;

    @GetMapping("/")
    public List<Location> getAll() {
        return locationServiceImpl.getAllLocations();
    }

    @GetMapping("/{id}")
    public Location getById(@PathVariable("id") final String id) {
        return locationServiceImpl.getLocationById(id);
    }


    @PostMapping("/")
    public Location create(@RequestBody Location location) {
        return locationServiceImpl.insertLocation(location);
    }

    @PutMapping("/{id}")
    public Location update(@PathVariable("id") final String id, @RequestBody Location location) {
        return locationServiceImpl.updateLocationById(id, location);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") final String id) {
        locationServiceImpl.deleteLocationById(id);
    }
}

