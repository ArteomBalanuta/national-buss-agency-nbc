package com.endava.nbs.web;

import com.endava.nbs.domain.dto.TicketsPageDto;
import com.endava.nbs.domain.service.impl.TicketsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/tickets")
public class TicketsController {

    private final TicketsService ticketsService;

    @GetMapping("/")
    public TicketsPageDto getTicketsPage(@RequestParam int offset, @RequestParam int limit) {
        return ticketsService.getTicketsPage(offset, limit);
    }
}
