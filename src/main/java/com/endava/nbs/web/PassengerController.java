package com.endava.nbs.web;

import com.endava.nbs.domain.entity.Passenger;
import com.endava.nbs.domain.service.PassengerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/passenger")
@RequiredArgsConstructor
public class PassengerController {

    private final PassengerService passengerService;

    @GetMapping("/")
    public List<Passenger> getAll() throws SQLException {
        return passengerService.getAllPassengers();
    }

    @GetMapping("/{id}")
    public Passenger getById(@PathVariable("id") final String id) throws SQLException {
        return passengerService.getPassengerById(id);
    }

    @PostMapping("/")
    public void create(@RequestBody Passenger passenger) throws SQLException {
        passengerService.insertPassenger(passenger);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Passenger> deleteById(@PathVariable("id") String id) throws SQLException {
        passengerService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/{id}")
    public Passenger updatePassenger(@PathVariable("id") String id, @RequestBody Passenger passenger) throws SQLException {
        return passengerService.updatePassengerById(id, passenger);
    }
}
