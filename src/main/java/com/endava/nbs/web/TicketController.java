package com.endava.nbs.web;

import com.endava.nbs.domain.dto.TicketDtoIn;
import com.endava.nbs.domain.dto.TicketDtoOut;
import com.endava.nbs.domain.service.TicketService;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/ticket")
public class TicketController {

    private final TicketService ticketService;

    @GetMapping("/")
    public List<TicketDtoOut> getAll() {
        return ticketService.getAllTickets();
    }

    @GetMapping("/{id}")
    public TicketDtoOut getById(@PathVariable("id") final Integer id) {
        return ticketService.getTicketById(id);
    }

    @PostMapping
    public TicketDtoOut create(@ApiParam(name = "Ticket", value = "Data for booking a ticket", required = true)
                               @Valid @RequestBody TicketDtoIn ticketDtoIn) throws SQLException {
        return ticketService.insertTicket(ticketDtoIn);
    }

    @PutMapping("/{id}")
    public TicketDtoOut update(@PathVariable("id") final Integer id,
                               @ApiParam(name = "Ticket", value = "Data for updating a ticket", required = true)
                               @Valid @RequestBody TicketDtoIn ticketDtoIn) throws SQLException {
        return ticketService.updateTicketById(id, ticketDtoIn);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") final Integer id) {
        ticketService.deleteTicketById(id);
    }
}
