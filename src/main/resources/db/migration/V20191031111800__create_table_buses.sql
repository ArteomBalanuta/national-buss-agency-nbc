create TABLE Buses
(
idBus NUMBER (6) PRIMARY KEY,
factoryNR VARCHAR (30) NOT NULL,
typeBus VARCHAR (30) NOT NULL,
capacityTotal NUMBER(3) NOT NULL,
capacityEconom NUMBER(3),
capacityBusiness NUMBER(3),
timeInspection DATE NOT NULL
);

COMMENT ON TABLE Buses IS 'Busses table. References with tickets table.';
COMMENT ON COLUMN Buses.idBus IS 'Primary key of busses table.';
COMMENT ON COLUMN Buses.factoryNR IS 'factoryNR - of machine';
COMMENT ON COLUMN Buses.typeBus IS 'MINIVAN, STANDARD, DOUBLE-DECKER ';
COMMENT ON COLUMN Buses.capacityTotal IS 'Total capacity of buss ';
COMMENT ON COLUMN Buses.capacityEconom IS 'Capacity of econom seats in buss ';
COMMENT ON COLUMN Buses.capacityBusiness IS 'Capacity of business seats in buss ';
COMMENT ON COLUMN Buses.timeInspection IS 'Last time inspection date';