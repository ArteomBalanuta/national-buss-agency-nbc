alter table routes
add CONSTRAINT check_date
  CHECK (depTime < arrivalTime);