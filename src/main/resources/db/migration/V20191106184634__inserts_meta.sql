--Locations
INSERT INTO Locations(idLocation, cityName) VALUES (1,'Chisinau');
INSERT INTO Locations(idLocation, cityName) VALUES (2,'Orhei');
INSERT INTO Locations(idLocation, cityName) VALUES (3,'Balti');
INSERT INTO Locations(idLocation, cityName) VALUES (4,'City1');
INSERT INTO Locations(idLocation, cityName) VALUES (5,'City3');
INSERT INTO Locations(idLocation, cityName) VALUES (6,'City4');

--Buses
INSERT INTO
BUSES(idBus,factoryNr,typeBus,capacityTotal,capacityEconom,capacityBusiness,timeInspection)
VALUES (1,'MFnumberT125','MINIVAN',20,10,10,TO_DATE('12-OCT-2019 14:00:02','DD-MM-YYYY hh24-mi-ss'));
INSERT INTO
BUSES(idBus,factoryNr,typeBus,capacityTotal,capacityEconom,capacityBusiness,timeInspection)
VALUES (2,'SFnumberT125','STANDARD',30,15,15,TO_DATE('12-OCT-2019 14:00:02','DD-MM-YYYY hh24-mi-ss'));
INSERT INTO
BUSES(idBus,factoryNr,typeBus,capacityTotal,capacityEconom,capacityBusiness,timeInspection)
VALUES (3,'DFnumberT125','DOUBLE-DECKER',50,25,25,TO_DATE('12-OCT-2019 14:30:05','DD-MM-YYYY hh24-mi-ss'));
INSERT INTO
BUSES(idBus,factoryNr,typeBus,capacityTotal,capacityEconom,capacityBusiness,timeInspection)
VALUES (4,'DFnumberT126','DOUBLE-DECKER',50,25,25,TO_DATE('12-OCT-2019 14:20:05','DD-MM-YYYY hh24-mi-ss'));

--Routes
INSERT INTO ROUTES(idroute,srclocation,destlocation,deptime,arrivaltime,idbus,soldbusiness,soldeconomy)
VALUES ('6f2bdaec-ffc2-485e-b8b1-54e1566d5e10',1,2,TO_DATE('12-OCT-2019 14:00:02','DD-MM-YYYY hh24-mi-ss'),TO_DATE('12-OCT-2019 15:00:01','DD-MM-YYYY hh24-mi-ss'),1,0,0);
INSERT INTO ROUTES(idroute,srclocation,destlocation,deptime,arrivaltime,idbus,soldbusiness,soldeconomy)
VALUES ('085b874b-f518-455f-b136-e9e269219a35',2,3,TO_DATE('12-OCT-2019 14:30:03','DD-MM-YYYY hh24-mi-ss'),TO_DATE('12-OCT-2019 15:30:01','DD-MM-YYYY hh24-mi-ss'),2,0,0);
INSERT INTO ROUTES(idroute,srclocation,destlocation,deptime,arrivaltime,idbus,soldbusiness,soldeconomy)
VALUES ('7a7793fc-3df7-4212-8ac2-6e7c9dc9f98c',2,3,TO_DATE('12-OCT-2019 14:30:03','DD-MM-YYYY hh24-mi-ss'),TO_DATE('12-OCT-2019 15:30:01','DD-MM-YYYY hh24-mi-ss'),2,0,0);


--Passengers
INSERT INTO PASSENGERS(idPassport,firstName,lastName,birthDate) VALUES ('1967001052123','Joseph','Acaba',TO_DATE('17-MAY-1967'));
INSERT INTO PASSENGERS(idPassport,firstName,lastName,birthDate) VALUES ('193601052123','Loren','Acton',TO_DATE('07-MAR-1936'));
INSERT INTO PASSENGERS(idPassport,firstName,lastName,birthDate) VALUES ('193001094023','Michael','Adams',TO_DATE('05-MAY-1930'));