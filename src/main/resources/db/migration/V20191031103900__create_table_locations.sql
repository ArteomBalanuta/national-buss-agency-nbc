create TABLE Locations
(
    idLocation VARCHAR2(3) PRIMARY KEY,
    cityName   VARCHAR2(30) NOT NULL
);