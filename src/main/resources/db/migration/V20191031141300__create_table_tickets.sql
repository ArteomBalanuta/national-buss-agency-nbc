create TABLE Tickets
(
    idTicket    NUMBER(8) PRIMARY KEY,
    idPassport  VARCHAR2(13) NOT NULL,
    idRoute     VARCHAR(36)      NOT NULL,
    class       VARCHAR2(3)  NOT NULL,
    price       NUMBER(7, 2) NOT NULL,
    seatNr      NUMBER(3)    NOT NULL,
    luggageType VARCHAR2(3)  NOT NULL,

    CONSTRAINT tickets_class_in CHECK (class IN ('ECO', 'BUS')),
    CONSTRAINT tickets_luggageType_in CHECK (luggageType IN ('SML', 'LRG')),
    CONSTRAINT tickets_price_min CHECK (price > 0)
);

create sequence tickets_seq nocache;

alter table Tickets
    modify idTicket DEFAULT tickets_seq.nextval;