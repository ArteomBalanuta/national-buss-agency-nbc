create TABLE Routes
(
    idRoute        VARCHAR(36)       UNIQUE NOT NULL,
    srcLocation    VARCHAR(3)        NOT NULL,
    destLocation   VARCHAR(3)        NOT NULL,
    depTime        DATE              NOT NULL,
    arrivalTime    DATE,
    idBus          NUMBER(6)         NOT NULL,
    soldBusiness   NUMBER(3),
    soldEconomy    NUMBER(3)
);

--------------------------------------------------------
--  Ref Constraints for Table ROUTES
--------------------------------------------------------

alter table Routes
    add CONSTRAINT ROUTESRC_REG_FK FOREIGN KEY (srcLocation)
        REFERENCES Locations (idLocation);

alter table Routes
    add CONSTRAINT ROUTEDEST_REG_FK FOREIGN KEY (destLocation)
        REFERENCES Locations (idLocation);