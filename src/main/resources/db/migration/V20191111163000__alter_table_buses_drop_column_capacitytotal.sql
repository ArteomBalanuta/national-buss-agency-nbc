CREATE SEQUENCE buses_seq nocache;

ALTER TABLE BUSES
    DROP COLUMN capacityTotal;

ALTER TABLE BUSES
    MODIFY idBus DEFAULT buses_seq.nextval;
