CREATE table Passengers(
idPassport VARCHAR (13) PRIMARY KEY,
firstName VARCHAR (30) NOT NULL,
lastName  VARCHAR (30)  NOT NULL,
birthDate DATE NOT NULL
);

COMMENT ON TABLE Passengers IS 'Passenger table. References with locations table.';
COMMENT ON COLUMN Passengers.idPassport IS 'Primary key of passengers table.';
COMMENT ON COLUMN Passengers.firstName IS 'First name of the passenger. A not null column.';
COMMENT ON COLUMN Passengers.lastName IS 'Last name of the passenger. A not null column.';
COMMENT ON COLUMN Passengers.birthDate IS 'birth date  of the passenger. A not null column.';
