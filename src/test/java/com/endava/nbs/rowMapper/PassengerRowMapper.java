package com.endava.nbs.rowMapper;

import com.endava.nbs.domain.entity.Passenger;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PassengerRowMapper implements RowMapper<Passenger> {

    private static final String ID_PASSPORT = "idPassport";
    private static final String FIRST_NAME = "firstName";
    private static final String LAST_NAME = "lastName";
    private static final String BIRTH_DATE = "birthDate";

    @Override
    public Passenger mapRow(ResultSet rs, int rowNum) throws SQLException {
        Passenger passenger = new Passenger();
        passenger.setIdPassport(rs.getString(ID_PASSPORT));
        passenger.setFirstName(rs.getString(FIRST_NAME));
        passenger.setLastName(rs.getString(LAST_NAME));
        passenger.setBirthDate(LocalDate.parse(rs.getString(BIRTH_DATE), DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")));
        return passenger;
    }
}