package com.endava.nbs.rowMapper;

import com.endava.nbs.domain.entity.Bus;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class BusRowMapper implements RowMapper<Bus> {
    @Override
    public Bus mapRow(ResultSet rs, int rowNum) throws SQLException {
        Bus bus = new Bus();

        bus.setIdBus(rs.getInt("idBus"));
        bus.setFactoryNR(rs.getString("factoryNR"));
        bus.setTypeBus(rs.getString("typeBus"));
        bus.setCapacityEconom(rs.getByte("capacityEconom"));
        bus.setCapacityBusiness(rs.getByte("capacityBusiness"));
        bus.setTimeInspection(LocalDateTime.parse(rs.getString("timeInspection"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        return bus;
    }
}
