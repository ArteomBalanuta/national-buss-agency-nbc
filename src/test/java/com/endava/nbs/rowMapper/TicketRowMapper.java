package com.endava.nbs.rowMapper;

import com.endava.nbs.domain.dao.PassengerRepository;
import com.endava.nbs.domain.dao.RouteRepository;
import com.endava.nbs.domain.entity.Ticket;
import com.endava.nbs.domain.exception.BadRequestException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
@RequiredArgsConstructor
public class TicketRowMapper implements RowMapper<Ticket> {

    private final PassengerRepository passengerRepository;

    private final RouteRepository routeRepository;

    @Override
    public Ticket mapRow(ResultSet rs, int rowNum) throws SQLException {
        Ticket ticket = new Ticket();

        ticket.setIdTicket(rs.getInt("idTicket"));
        ticket.setPassenger(passengerRepository.findById(rs.getString("idPassport")).
                orElseThrow(() -> new BadRequestException(HttpStatus.NOT_FOUND, "Passenger not found (TicketRowMapper)")));
        ticket.setRoute(routeRepository.getRouteById(rs.getString("idRoute")).
                orElseThrow(() -> new BadRequestException(HttpStatus.NOT_FOUND, "Route not found (TicketRowMapper)")));
        ticket.setPrice(rs.getDouble("price"));
        ticket.setSeatNr(rs.getInt("seatNr"));
        ticket.setLuggageType(rs.getString("luggageType"));
        ticket.setClassType(rs.getString("class"));

        return ticket;
    }
}
