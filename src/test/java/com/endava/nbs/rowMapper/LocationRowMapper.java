package com.endava.nbs.rowMapper;

import com.endava.nbs.domain.entity.Location;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LocationRowMapper implements RowMapper<Location> {

    @Override
    public Location mapRow(ResultSet rs, int rowNum) throws SQLException {
        Location location = new Location();

        location.setIdLocation(rs.getString("idLocation"));
        location.setCityName(rs.getString("cityName"));

        return location;
    }
}
