package com.endava.nbs.web;


import com.endava.nbs.config.TestConfig;
import com.endava.nbs.domain.service.impl.RouteServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@WebMvcTest(controllers = RouteController.class)
@Import(TestConfig.class)
abstract public class BaseControllerTest {

    @Autowired
    protected MockMvc mockMvc;

    public static final MediaType JSON_UTF8 = new MediaType(
            APPLICATION_JSON.getType(),
            APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @MockBean
    RouteServiceImpl routeServiceMock;

    @AfterEach
    void verifyMockedResources() {
        verifyNoMoreInteractions(routeServiceMock);
    } //all mocks should be added here also!
}