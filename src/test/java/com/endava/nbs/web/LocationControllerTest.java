package com.endava.nbs.web;

import com.endava.nbs.config.TestConfig;
import com.endava.nbs.domain.entity.Location;
import com.endava.nbs.domain.service.LocationService;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {LocationController.class})
@Import(TestConfig.class)
public class LocationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectWriter objectWriter;

    @MockBean
    LocationService locationService;

    public static final MediaType JSON_UTF8 = new MediaType(APPLICATION_JSON.getType(), APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);
    private final Location location = new Location("CHI", "Chisinau");

    @Test
    public void testGetAll() throws Exception {
        when(locationService.getAllLocations()).thenReturn(Arrays.asList(new Location("CHI", "Chisinau"), new Location("BAL", "Balti")));

        mockMvc.perform(get("/location/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].idLocation", containsInAnyOrder("CHI", "BAL")))
                .andExpect(jsonPath("$[*].cityName", containsInAnyOrder("Chisinau", "Balti")));

        verify(locationService).getAllLocations();
    }

    @Test
    public void testGetById() throws Exception {
        when(locationService.getLocationById(location.getIdLocation())).thenReturn(location);

        mockMvc.perform(get("/location/{id}", location.getIdLocation()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idLocation", is("CHI")))
                .andExpect(jsonPath("$.cityName", is("Chisinau")));

        verify(locationService).getLocationById("CHI");
    }

    @Test
    public void testCreate() throws Exception {

        String locationJson = objectWriter.writeValueAsString(location);

        mockMvc.perform(post("/location/")
                .contentType(JSON_UTF8)
                .content(locationJson))
                .andDo(print())
                .andExpect(status().isOk());

        verify(locationService).insertLocation(location);
    }

    @Test
    public void testUpdate() throws Exception {

        String locationJson = objectWriter.writeValueAsString(location);
        when(locationService.updateLocationById(location.getIdLocation(), location)).thenReturn(location);

        mockMvc.perform(put("/location/{id}", location.getIdLocation())
                .contentType(JSON_UTF8)
                .content(locationJson))
                .andDo(print())
                .andExpect(jsonPath("$.idLocation", is("CHI")))
                .andExpect(jsonPath("$.cityName", is("Chisinau")))
                .andExpect(status().isOk());

        verify(locationService).updateLocationById(location.getIdLocation(), location);
    }

    @Test
    public void testDelete() throws Exception {

        mockMvc.perform(delete("/location/{id}", location.getIdLocation()))
                .andDo(print())
                .andExpect(status().isOk());

        verify(locationService).deleteLocationById(location.getIdLocation());
    }
}