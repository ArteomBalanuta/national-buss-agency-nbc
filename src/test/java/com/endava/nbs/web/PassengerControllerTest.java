package com.endava.nbs.web;

import com.endava.nbs.config.TestConfig;
import com.endava.nbs.domain.dto.PassengerDto;
import com.endava.nbs.domain.entity.Passenger;
import com.endava.nbs.domain.service.PassengerService;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.UUID;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {PassengerController.class})
@Import(TestConfig.class)
public class PassengerControllerTest extends BaseControllerTest {
    @Autowired
    private ObjectWriter objectWriter;

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PassengerService passengerService;

    private PassengerDto dtoPassenger;
    private String passengerId;

    @BeforeEach
    void setUp() {
        dtoPassenger = PassengerDto.builder()
                .idPassport(UUID.randomUUID().toString())
                .idPassport(UUID.randomUUID().toString())
                .firstName("Radu")
                .lastName("Gribincea")
                .birthDate(LocalDate.of(1958, 05, 19))
                .build();
        passengerId = dtoPassenger.getIdPassport();
    }

    @Test
    void getAllPassengers() throws Exception {
        Passenger firstPassenger = Passenger.builder()
                .idPassport("0023451209")
                .firstName("Radu")
                .lastName("Gribincea")
                .birthDate(LocalDate.of(1958, 05, 19))
                .build();

        Passenger secondPassenger = Passenger.builder()
                .idPassport("0023451200")
                .firstName("Andrei")
                .lastName("Cusnir")
                .birthDate(LocalDate.of(1998, 10, 19))
                .build();

        when(passengerService.getAllPassengers()).thenReturn(asList(firstPassenger, secondPassenger));
        mockMvc.perform(get("/passenger"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].idPassenger", containsInAnyOrder(firstPassenger.getIdPassport(), secondPassenger.getIdPassport())));
        verify(passengerService).getAllPassengers();
    }
}
