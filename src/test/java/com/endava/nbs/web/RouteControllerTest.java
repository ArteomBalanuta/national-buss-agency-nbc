package com.endava.nbs.web;


import com.endava.nbs.config.TestConfig;
import com.endava.nbs.domain.dto.RouteDto;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;

import java.time.LocalDateTime;
import java.util.UUID;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {RouteController.class})
@Import(TestConfig.class)
public class RouteControllerTest extends BaseControllerTest {

    @Autowired
    private ObjectWriter objectWriter;

    private RouteDto routeDto;
    private String routeId;

    @BeforeEach
    void setUp() {
        routeDto = RouteDto.builder()
                .idRoute(UUID.randomUUID().toString())
                .locationDest("BAL")
                .locationSrc("BEN")
                .timeDeparture(LocalDateTime.of(2019, 12, 10, 14, 0, 0, 2))
                .timeArrival(LocalDateTime.of(2021, 12, 10, 15, 0, 0, 1))
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();
        routeId = routeDto.getIdRoute();
    }

    @Test
    void getAllRoutes200() throws Exception {
        RouteDto firstRoute = RouteDto.builder()
                .idRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10")
                .idBus(1L)
                .locationSrc("CHI")
                .locationDest("BAL")
                .timeDeparture(LocalDateTime.of(2019, 12, 10, 14, 0, 0, 2))
                .timeArrival(LocalDateTime.of(2021, 12, 10, 15, 0, 0, 1))
                .soldBussines(0L)
                .soldEconomy(0L)
                .build();

        RouteDto secondRoute = RouteDto.builder()
                .idRoute("085b874b-f518-455f-b136-e9e269219a35")
                .idBus(2L)
                .locationSrc("ORH")
                .locationDest("CHI")
                .timeDeparture(LocalDateTime.of(2019, 12, 10, 14, 0, 0, 2))
                .timeArrival(LocalDateTime.of(2021, 12, 10, 15, 0, 0, 1))
                .soldBussines(0L)
                .soldEconomy(0L)
                .build();

        when(routeServiceMock.getAllRoutes()).thenReturn(asList(firstRoute, secondRoute));
        mockMvc.perform(get("/route"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].idRoute", containsInAnyOrder(firstRoute.getIdRoute(), secondRoute.getIdRoute())))
                .andExpect(jsonPath("$[*].idBus", containsInAnyOrder(Math.toIntExact(firstRoute.getIdBus()), Math.toIntExact(secondRoute.getIdBus()))));

        verify(routeServiceMock).getAllRoutes();
    }

    @Test
    void getByRouteId200() throws Exception {

        when(routeServiceMock.getRouteById(routeId)).thenReturn(routeDto);

        mockMvc.perform(get("/route/{routeId}", routeId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idRoute").value(routeDto.getIdRoute()));

        verify(routeServiceMock).getRouteById(routeId);
    }

    //TODO: fix date string format!!
    @Test
    void postNewRoute201() throws Exception {
        when(routeServiceMock.insertRoute(routeDto)).thenReturn(routeDto);

        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        final String requestJson = objectWriter.writeValueAsString(routeDto);

        mockMvc.perform(
                post("/route")
                        .contentType(JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk());
        //.andExpect(content().contentType(JSON_UTF8))
        //.andExpect(jsonPath("$.idRoute", equalTo(routeDto.getIdRoute())));

        verify(routeServiceMock).insertRoute(routeDto);
    }

    //TODO: fix date string format!!
    @Test
    void putRoute202() throws Exception {
        final String requestJson = objectWriter.writeValueAsString(routeDto);

        when(routeServiceMock.updateRouteById(routeDto.getIdRoute(), routeDto)).thenReturn(routeDto);
        mockMvc.perform(
                put("/route/{routeId}", routeId)
                        .contentType(JSON_UTF8)
                        .content(requestJson))
                .andDo(print())
                .andExpect(status().isOk());
        //.andExpect(jsonPath("$.routeId", equalTo(routeDto.getIdRoute())));

        verify(routeServiceMock).updateRouteById(routeId, routeDto);
    }

    @Test
    void shouldDeleteExistingRoute() throws Exception {
        mockMvc.perform(delete("/route/{routeId}", routeId))
                .andDo(print())
                .andExpect(status().isOk());

        verify(routeServiceMock).deleteRouteById(routeId);
    }
}