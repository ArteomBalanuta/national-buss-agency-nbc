package com.endava.nbs.web;

import com.endava.nbs.config.TestConfig;
import com.endava.nbs.domain.dto.TicketDtoIn;
import com.endava.nbs.domain.dto.TicketDtoOut;
import com.endava.nbs.domain.service.TicketService;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {TicketController.class})
@Import(TestConfig.class)
public class TicketControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectWriter objectWriter;

    @MockBean
    TicketService ticketService;

    public static final MediaType JSON_UTF8 = new MediaType(APPLICATION_JSON.getType(), APPLICATION_JSON.getSubtype(), StandardCharsets.UTF_8);

    private final TicketDtoIn ticketDtoIn;
    private final TicketDtoOut ticketDtoOut1;
    private final TicketDtoOut ticketDtoOut2;

    {
        ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("BUS");
        ticketDtoIn.setLuggageType("LRG");

        ticketDtoOut1 = new TicketDtoOut();
        ticketDtoOut1.setIdTicket(1);
        ticketDtoOut1.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoOut1.setIdPassport("0023451200");
        ticketDtoOut1.setClassType("BUS");
        ticketDtoOut1.setLuggageType("LRG");
        ticketDtoOut1.setSeatNr(1);
        ticketDtoOut1.setPrice(150d);

        ticketDtoOut2 = new TicketDtoOut();
        ticketDtoOut2.setIdTicket(2);
        ticketDtoOut2.setIdRoute("085b874b-f518-455f-b136-e9e269219a35");
        ticketDtoOut2.setIdPassport("1123451209");
        ticketDtoOut2.setClassType("ECO");
        ticketDtoOut2.setLuggageType("SML");
        ticketDtoOut2.setSeatNr(11);
        ticketDtoOut2.setPrice(100d);
    }

    @Test
    public void testGetAll() throws Exception {
        when(ticketService.getAllTickets()).thenReturn(Arrays.asList(ticketDtoOut1, ticketDtoOut2));

        mockMvc.perform(get("/ticket/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[*].idTicket", containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$[*].idRoute", containsInAnyOrder("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10", "085b874b-f518-455f-b136-e9e269219a35")))
                .andExpect(jsonPath("$[*].idPassport", containsInAnyOrder("0023451200", "1123451209")))
                .andExpect(jsonPath("$[*].price", containsInAnyOrder(100d, 150d)));

        verify(ticketService).getAllTickets();
    }

    @Test
    public void testGetById() throws Exception {
        when(ticketService.getTicketById(ticketDtoOut1.getIdTicket())).thenReturn(ticketDtoOut1);

        mockMvc.perform(get("/ticket/{id}", ticketDtoOut1.getIdTicket()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.idTicket", is(1)))
                .andExpect(jsonPath("$.idRoute", is("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10")))
                .andExpect(jsonPath("$.idPassport", is("0023451200")))
                .andExpect(jsonPath("$.classType", is("BUS")))
                .andExpect(jsonPath("$.luggageType", is("LRG")))
                .andExpect(jsonPath("$.seatNr", is(1)))
                .andExpect(jsonPath("$.price", is(150d)));

        verify(ticketService).getTicketById(ticketDtoOut1.getIdTicket());
    }

    @Test
    public void testCreate() throws Exception {

        String ticketJson = objectWriter.writeValueAsString(ticketDtoIn);

        mockMvc.perform(post("/ticket/")
                .contentType(JSON_UTF8)
                .content(ticketJson))
                .andDo(print())
                .andExpect(status().isOk());

        verify(ticketService).insertTicket(ticketDtoIn);
    }

    @Test
    public void testUpdate() throws Exception {

        String ticketJson = objectWriter.writeValueAsString(ticketDtoIn);
        when(ticketService.updateTicketById(1, ticketDtoIn)).thenReturn(ticketDtoOut1);

        mockMvc.perform(put("/ticket/{id}", 1)
                .contentType(JSON_UTF8)
                .content(ticketJson))
                .andDo(print())
                .andExpect(jsonPath("$.idTicket", is(1)))
                .andExpect(jsonPath("$.idRoute", is("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10")))
                .andExpect(jsonPath("$.idPassport", is("0023451200")))
                .andExpect(jsonPath("$.classType", is("BUS")))
                .andExpect(jsonPath("$.luggageType", is("LRG")))
                .andExpect(jsonPath("$.seatNr", is(1)))
                .andExpect(jsonPath("$.price", is(150d)))
                .andExpect(status().isOk());

        verify(ticketService).updateTicketById(1, ticketDtoIn);
    }

    @Test
    public void testDelete() throws Exception {

        mockMvc.perform(delete("/ticket/{id}", 1))
                .andDo(print())
                .andExpect(status().isOk());

        verify(ticketService).deleteTicketById(1);
    }
}