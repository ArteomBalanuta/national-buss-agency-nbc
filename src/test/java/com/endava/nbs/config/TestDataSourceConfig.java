package com.endava.nbs.config;

import oracle.jdbc.datasource.OracleDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
public class TestDataSourceConfig {

    @Bean
    DataSource dataSource() throws SQLException {
        OracleDataSource dataSource = new oracle.jdbc.pool.OracleDataSource();
        dataSource.setUser("nbsuser");
        dataSource.setPassword("nbspassword");
        dataSource.setURL("jdbc:oracle:thin:@//localhost:1521/orclpdb");
        dataSource.setImplicitCachingEnabled(true);
        return dataSource;
    }

    @Bean
    JdbcTemplate jdbcTemplate() throws SQLException {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws SQLException {
        LocalContainerEntityManagerFactoryBean em
                = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource());
        em.setPackagesToScan(new String[]{"com.endava.nbs"});

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);

        return em;
    }
}