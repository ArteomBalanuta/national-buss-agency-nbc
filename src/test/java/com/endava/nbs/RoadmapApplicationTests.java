package com.endava.nbs;

import com.endava.nbs.config.TestConfig;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = TestConfig.class)
public class RoadmapApplicationTests {

    @Test
    public void contextLoads() {

    }
}

