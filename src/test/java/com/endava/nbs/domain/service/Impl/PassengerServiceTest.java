package com.endava.nbs.domain.service.impl;


import com.endava.nbs.domain.entity.Passenger;
import com.endava.nbs.domain.service.impl.PassengerServiceImpl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)

public class PassengerServiceTest {
    @InjectMocks
    PassengerServiceImpl passengerService;
    @Mock
    PassengerServiceImpl passengerRepository;

    @Test
    void testInsertPassenger() {

        Passenger passenger = new Passenger();
        passenger.setIdPassport("0023451209");
        passenger.setFirstName("Radu");
        passenger.setLastName("Gribincea");
        passenger.setBirthDate(LocalDate.of(1958, 05, 19));

        passengerService.insertPassenger(passenger);
        verify(passengerRepository).insertPassenger(passenger);
    }

    @Test
    void testDeletePassengerById() {
        passengerService.deleteById("0023951200");
        verify(passengerRepository).deleteById("0023951200");
    }

    @Test
    void testGetLocationById() {
        Passenger passenger = new Passenger();
        passenger.setIdPassport("1023451209");
        passenger.setFirstName("Arteom");
        passenger.setLastName("Balanuta");
        passenger.setBirthDate(LocalDate.of(1988, 04, 11));
        Optional<Passenger> optionalPassenger = Optional.of(passenger);

        when(passengerRepository.getPassengerById("1023451209")).thenReturn(passenger);

        passengerService.getPassengerById("1023451209");
        verify(passengerRepository).getPassengerById("1023451209");
    }

    @Test
    void testGetAllPassengers() {
        passengerService.getAllPassengers();
        verify(passengerRepository).getAllPassengers();
    }

    @Test
    void testUpdatePassengerById() {
        Passenger passenger = new Passenger();
        passenger.setIdPassport("1023451209");
        passenger.setFirstName("Ion");
        passenger.setLastName("Cusnir");
        passenger.setBirthDate(LocalDate.of(1988,04,11));
        Optional<Passenger> optionalPassenger = Optional.of(passenger);

        when(passengerRepository.updatePassengerById("1023451209", passenger)).thenReturn(passenger);

        passengerService.updatePassengerById("1023451209", passenger);
        verify(passengerRepository).updatePassengerById("1023451209", passenger);
    }
}
