package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.LocationRepository;
import com.endava.nbs.domain.entity.Location;
import com.endava.nbs.domain.exception.BadRequestException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class LocationServiceTest {

    @InjectMocks
    LocationServiceImpl locationService;

    @Mock
    LocationRepository locationRepository;

    @Test
    void testInsertLocation() {
        Location location = new Location();
        location.setIdLocation("TIR");
        location.setCityName("Tiraspol");

        locationService.insertLocation(location);
        verify(locationRepository).insertLocation(location);
    }

    @Test
    void testDeleteLocationById() {
        Location location = new Location();
        location.setIdLocation("CHI");
        location.setCityName("Chisinau");
        Optional<Location> opLocation = Optional.of(location);

        when(locationRepository.getLocationById("CHI")).thenReturn(opLocation);
        locationService.deleteLocationById("CHI");
        verify(locationRepository).deleteLocationById("CHI");
    }

    @Test
    void testGetLocationById() {
        Location location = new Location();
        location.setIdLocation("TIR");
        location.setCityName("Tiraspol");
        Optional<Location> opLocation = Optional.of(location);

        when(locationRepository.getLocationById("TIR")).thenReturn(opLocation);

        locationService.getLocationById("TIR");
        verify(locationRepository).getLocationById("TIR");
    }

    @Test
    void testGetAllLocations() {
        locationService.getAllLocations();
        verify(locationRepository).getAllLocations();
    }

    @Test
    void testUpdateLocationById() {
        Location location = new Location();
        location.setIdLocation("BEN");
        location.setCityName("Tighina");
        Optional<Location> opLocation = Optional.of(location);

        when(locationRepository.getLocationById("BEN")).thenReturn(opLocation);

        locationService.updateLocationById("BEN", location);

        verify(locationRepository).insertLocation(location);
    }

    @Test
    void test_getLocationById_ShouldThrowException_WhenLocationNotFound() {

        when(locationRepository.getLocationById("OCN")).thenReturn(Optional.ofNullable(null));

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> locationService.getLocationById("OCN")).withMessage("404 Location with id: OCN NOT FOUND");
    }

    @Test
    void test_insertLocation_ShouldThrowException_WhenLocationAlreadyInDB() {
        Location location = new Location();
        location.setIdLocation("BEN");
        location.setCityName("Tighina");
        Optional<Location> opLocation = Optional.of(location);

        when(locationRepository.getLocationById(location.getIdLocation())).thenReturn(opLocation);

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> locationService.insertLocation(location)).withMessage("400 Location with id '" + location.getIdLocation() + "' ALREADY EXISTS");
    }

    @Test
    void test_updateLocationById_ShouldThrowException_WhenLocationNotFound() {

        when(locationRepository.getLocationById("TIR")).thenReturn(Optional.empty());

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> locationService.updateLocationById("TIR", new Location("TIR", "Tiraspol"))).
                withMessage("404 Location with id: 'TIR' NOT FOUND");
    }

    @Test
    void test_validateLocationId_ShouldThrowException_WhenLocationIdIsNull() {

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> locationService.getLocationById(null)).withMessage("400 Incorrect input Json");
    }

    @Test
    void test_validateLocationId_ShouldThrowException_WhenLocationIdIsInvalid() {
        String locationId = "CHIS";
        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> locationService.getLocationById(locationId)).withMessage("400 Location id should be 3 characters long. The inserted id has " + locationId.length() + " characters");
    }
}
