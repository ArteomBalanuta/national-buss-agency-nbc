package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.BusRepository;
import com.endava.nbs.domain.dao.RouteRepository;
import com.endava.nbs.domain.dao.TicketRepository;
import com.endava.nbs.domain.dto.TicketDtoIn;
import com.endava.nbs.domain.entity.*;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.mapper.TicketMapper;
import com.endava.nbs.domain.mapper.impl.TicketMapperImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

@ContextConfiguration(classes = {TicketMapperImpl.class})
@ExtendWith(MockitoExtension.class)
class TicketServiceTest {

    @InjectMocks
    TicketServiceImpl ticketService;

    @Mock
    TicketRepository ticketRepository;

    @Mock
    RouteRepository routeRepository;

    @Mock
    BusRepository busRepository;

    @Mock
    TicketMapper ticketMapper;

    @Test
    void test_insertTicket_ShouldSetPrice150SeatNr1RouteSoldBusiness1_WhenTravelClassBUSAndLuggageLRG() {

        Location locationSrc = new Location("CHI", "Chisinau");
        Location locationDest = new Location("BAL", "Balti");

        TicketDtoIn ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("BUS");
        ticketDtoIn.setLuggageType("LRG");

        Route route = new Route();
        route.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        route.setLocationSrc(locationSrc);
        route.setLocationDest(locationDest);
        route.setIdBus(1L);
        route.setSoldBussines(0L);
        route.setSoldEconomy(0L);
        Optional<Route> opRoute = Optional.ofNullable(route);

        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        ticket.setRoute(route);
        ticket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        ticket.setClassType("BUS");
        ticket.setLuggageType("LRG");
        ticket.setSeatNr(1);
        ticket.setPrice(150d);

        Route updatedRoute = new Route();
        updatedRoute.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        updatedRoute.setLocationSrc(locationSrc);
        updatedRoute.setLocationDest(locationDest);
        updatedRoute.setIdBus(1L);
        updatedRoute.setSoldBussines(1L);
        updatedRoute.setSoldEconomy(0L);

        Bus bus = new Bus();
        bus.setIdBus(1);
        bus.setFactoryNR("MFnumberT125");
        bus.setTypeBus("MINIVAN");
        bus.setCapacityEconom((byte) 10);
        bus.setCapacityBusiness((byte) 10);
        bus.setTimeInspection(LocalDateTime.of(2019, 10, 12, 14, 0, 2));
        Optional<Bus> opBus = Optional.ofNullable(bus);

        when(ticketMapper.toEntity(ticketDtoIn)).thenReturn(ticket);
        when(routeRepository.getRouteById(ticketDtoIn.getIdRoute())).thenReturn(opRoute);
        when(busRepository.getBusById(1)).thenReturn(opBus);
        ticketService.insertTicket(ticketDtoIn);
        verify(ticketRepository).insertTicket(ticket);
        verify(routeRepository, times(2)).getRouteById(ticketDtoIn.getIdRoute());
        verify(routeRepository).updateRouteById(ticketDtoIn.getIdRoute(), updatedRoute);
    }

    @Test
    void test_insertTicket_ShouldSetPrice100SeatNr11RouteSoldEconomy1_WhenTravelClassECOAndLuggageSML() {

        Location locationSrc = new Location("CHI", "Chisinau");
        Location locationDest = new Location("BAL", "Balti");

        TicketDtoIn ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("ECO");
        ticketDtoIn.setLuggageType("SML");

        Route route = new Route();
        route.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        route.setLocationSrc(locationSrc);
        route.setLocationDest(locationDest);
        route.setIdBus(1L);
        route.setSoldBussines(0L);
        route.setSoldEconomy(0L);
        Optional<Route> opRoute = Optional.ofNullable(route);

        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        ticket.setRoute(route);
        ticket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        ticket.setClassType("ECO");
        ticket.setLuggageType("SML");
        ticket.setSeatNr(11);
        ticket.setPrice(100d);

        Route updatedRoute = new Route();
        updatedRoute.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        updatedRoute.setLocationSrc(locationSrc);
        updatedRoute.setLocationDest(locationDest);
        updatedRoute.setIdBus(1L);
        updatedRoute.setSoldBussines(0L);
        updatedRoute.setSoldEconomy(1L);

        Bus bus = new Bus();
        bus.setIdBus(1);
        bus.setFactoryNR("MFnumberT125");
        bus.setTypeBus("MINIVAN");
        bus.setCapacityEconom((byte) 10);
        bus.setCapacityBusiness((byte) 10);
        bus.setTimeInspection(LocalDateTime.of(2019, 10, 12, 14, 0, 2));
        Optional<Bus> opBus = Optional.ofNullable(bus);

        when(ticketMapper.toEntity(ticketDtoIn)).thenReturn(ticket);
        when(routeRepository.getRouteById(ticketDtoIn.getIdRoute())).thenReturn(opRoute);
        when(busRepository.getBusById(1)).thenReturn(opBus);
        ticketService.insertTicket(ticketDtoIn);
        verify(ticketRepository).insertTicket(ticket);
        verify(routeRepository, times(2)).getRouteById(ticketDtoIn.getIdRoute());
        verify(routeRepository).updateRouteById(ticketDtoIn.getIdRoute(), updatedRoute);
    }

    @Test
    void test_insertTicket_ShouldThrowException_WhenSoldBussinessClassTicketsExceedBusCapacity() {

        Location locationSrc = new Location("CHI", "Chisinau");
        Location locationDest = new Location("BAL", "Balti");

        TicketDtoIn ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("BUS");
        ticketDtoIn.setLuggageType("LRG");

        Route route = new Route();
        route.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        route.setLocationSrc(locationSrc);
        route.setLocationDest(locationDest);
        route.setIdBus(1L);
        route.setSoldBussines(10L);
        route.setSoldEconomy(0L);
        Optional<Route> opRoute = Optional.ofNullable(route);

        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        ticket.setRoute(route);
        ticket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        ticket.setClassType("BUS");
        ticket.setLuggageType("LRG");
        ticket.setSeatNr(1);
        ticket.setPrice(150d);

        Bus bus = new Bus();
        bus.setIdBus(1);
        bus.setFactoryNR("MFnumberT125");
        bus.setTypeBus("MINIVAN");
        bus.setCapacityEconom((byte) 10);
        bus.setCapacityBusiness((byte) 10);
        bus.setTimeInspection(LocalDateTime.of(2019, 10, 12, 14, 0, 2));
        Optional<Bus> opBus = Optional.ofNullable(bus);

        when(ticketMapper.toEntity(ticketDtoIn)).thenReturn(ticket);
        when(routeRepository.getRouteById(ticketDtoIn.getIdRoute())).thenReturn(opRoute);
        when(busRepository.getOne(1)).thenReturn(bus);

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> ticketService.insertTicket(ticketDtoIn)).withMessage("400 No Business Class tickets available on Route: " + ticketDtoIn.getIdRoute());
    }

    @Test
    void test_insertTicket_ShouldThrowException_WhenSoldEconomyClassTicketsExceedBusCapacity() {

        Location locationSrc = new Location("CHI", "Chisinau");
        Location locationDest = new Location("BAL", "Balti");

        TicketDtoIn ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("ECO");
        ticketDtoIn.setLuggageType("LRG");

        Route route = new Route();
        route.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        route.setLocationSrc(locationSrc);
        route.setLocationDest(locationDest);
        route.setIdBus(1L);
        route.setSoldBussines(0L);
        route.setSoldEconomy(10L);
        Optional<Route> opRoute = Optional.ofNullable(route);

        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        ticket.setRoute(route);
        ticket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        ticket.setClassType("BUS");
        ticket.setLuggageType("LRG");
        ticket.setSeatNr(1);
        ticket.setPrice(150d);

        Bus bus = new Bus();
        bus.setIdBus(1);
        bus.setFactoryNR("MFnumberT125");
        bus.setTypeBus("MINIVAN");
        bus.setCapacityEconom((byte) 10);
        bus.setCapacityBusiness((byte) 10);
        bus.setTimeInspection(LocalDateTime.of(2019, 10, 12, 14, 0, 2));
        Optional<Bus> opBus = Optional.ofNullable(bus);

        when(ticketMapper.toEntity(ticketDtoIn)).thenReturn(ticket);
        when(routeRepository.getRouteById(ticketDtoIn.getIdRoute())).thenReturn(opRoute);
        when(busRepository.getBusById(1)).thenReturn(opBus);

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> ticketService.insertTicket(ticketDtoIn)).withMessage("400 No Economy Class tickets available on Route: " + ticketDtoIn.getIdRoute());
    }

    @Test
    void test_insertTicket_ShouldThrowException_WhenRouteNotFound() {

        Location locationSrc = new Location("CHI", "Chisinau");
        Location locationDest = new Location("BAL", "Balti");

        TicketDtoIn ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("ECO");
        ticketDtoIn.setLuggageType("LRG");

        Route route = new Route();
        route.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        route.setLocationSrc(locationSrc);
        route.setLocationDest(locationDest);
        route.setIdBus(1L);
        route.setSoldBussines(0L);
        route.setSoldEconomy(10L);
        Optional<Route> opRoute = Optional.ofNullable(route);

        Ticket ticket = new Ticket();
        ticket.setIdTicket(1);
        ticket.setRoute(route);
        ticket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        ticket.setClassType("BUS");
        ticket.setLuggageType("LRG");
        ticket.setSeatNr(1);
        ticket.setPrice(150d);

        when(ticketMapper.toEntity(ticketDtoIn)).thenReturn(ticket);
        when(routeRepository.getRouteById(ticketDtoIn.getIdRoute())).thenReturn(Optional.empty());

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> ticketService.insertTicket(ticketDtoIn)).withMessage("404 No Route with id: " + ticketDtoIn.getIdRoute());
    }

    @Test
    void test_insertTicketById_ShouldThrowException_WhenTravelClassIncorrect() {

        TicketDtoIn ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("XXX");
        ticketDtoIn.setLuggageType("LRG");

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> ticketService.insertTicket(ticketDtoIn)).withMessage("400 Incorrect 'classType'. Acceptable values: 'BUS', 'ECO");
    }

    @Test
    void test_insertTicketById_ShouldThrowException_WhenLuggageTypeIncorrect() {

        TicketDtoIn ticketDtoIn = new TicketDtoIn();
        ticketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        ticketDtoIn.setIdPassport("0023451200");
        ticketDtoIn.setClassType("BUS");
        ticketDtoIn.setLuggageType("XXX");

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> ticketService.insertTicket(ticketDtoIn)).withMessage("400 Incorrect 'luggageType'. Acceptable values: 'LRG', 'SML");
    }

    @Test
    void test_deleteTicketById_ShouldCallRepositoryDelete_WhenServiceDeleteCalled() {

        when(ticketRepository.getTicketById(1)).thenReturn(Optional.of(new Ticket()));

        ticketService.deleteTicketById(1);

        verify(ticketRepository).deleteTicketById(1);
    }

    @Test
    void test_getTicketById_ShouldCallRepositoryGet_WhenServiceGetCalled() {

        when(ticketRepository.getTicketById(1)).thenReturn(Optional.of(new Ticket()));

        ticketService.getTicketById(1);

        verify(ticketRepository).getTicketById(1);
    }

    @Test
    void test_getTicketById_ShouldThrowException_WhenTicketNotFound() {

        when(ticketRepository.getTicketById(1)).thenReturn(Optional.empty());

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> ticketService.getTicketById(1)).withMessage("404 Ticket with id: 1 NOT FOUND");
    }

    @Test
    void test_getAllTickets() {
        ticketService.getAllTickets();
        verify(ticketRepository).getAllTickets();
    }

    @Test
    void test_updateTicketById_ShouldUpdateSeatNrPriceRoutesSoldTickets_WhenNewTravelClassEco() {

        Location locationSrc = new Location("CHI", "Chisinau");
        Location locationDest = new Location("BAL", "Balti");

        Route route = new Route();
        route.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        route.setLocationSrc(locationSrc);
        route.setLocationDest(locationDest);
        route.setIdBus(1L);
        route.setSoldBussines(1L);
        route.setSoldEconomy(0L);
        Optional<Route> opRoute = Optional.ofNullable(route);

        Ticket oldTicket = new Ticket();
        oldTicket.setIdTicket(1);
        oldTicket.setRoute(route);
        oldTicket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        oldTicket.setClassType("BUS");
        oldTicket.setLuggageType("LRG");
        oldTicket.setSeatNr(1);
        oldTicket.setPrice(150d);

        TicketDtoIn newTicketDtoIn = new TicketDtoIn();
        newTicketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        newTicketDtoIn.setIdPassport("0023451200");
        newTicketDtoIn.setClassType("ECO");
        newTicketDtoIn.setLuggageType("SML");

        Ticket newTicket = new Ticket();
        newTicket.setIdTicket(null);
        newTicket.setRoute(route);
        newTicket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        newTicket.setClassType("ECO");
        newTicket.setLuggageType("SML");
        newTicket.setSeatNr(null);
        newTicket.setPrice(null);

        Ticket updatedTicket = new Ticket();
        updatedTicket.setIdTicket(1);
        updatedTicket.setRoute(route);
        updatedTicket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        updatedTicket.setClassType("ECO");
        updatedTicket.setLuggageType("SML");
        updatedTicket.setSeatNr(11);
        updatedTicket.setPrice(100d);

        Route updatedRoute = new Route();
        updatedRoute.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        updatedRoute.setLocationSrc(locationSrc);
        updatedRoute.setLocationDest(locationDest);
        updatedRoute.setIdBus(1L);
        updatedRoute.setSoldBussines(0L);
        updatedRoute.setSoldEconomy(1L);

        Bus bus = new Bus();
        bus.setIdBus(1);
        bus.setFactoryNR("MFnumberT125");
        bus.setTypeBus("MINIVAN");
        bus.setCapacityEconom((byte) 10);
        bus.setCapacityBusiness((byte) 10);
        bus.setTimeInspection(LocalDateTime.of(2019, 10, 12, 14, 0, 2));
        Optional<Bus> opBus = Optional.ofNullable(bus);

        when(ticketMapper.toEntity(newTicketDtoIn)).thenReturn(newTicket);
        when(ticketRepository.getTicketById(1)).thenReturn(Optional.of(oldTicket));
        when(routeRepository.getRouteById(oldTicket.getRoute().getIdRoute())).thenReturn(opRoute);
        when(busRepository.getBusById(oldTicket.getRoute().getIdBus().intValue())).thenReturn(opBus);
        when(ticketRepository.insertTicket(updatedTicket)).thenReturn(updatedTicket);

        ticketService.updateTicketById(1, newTicketDtoIn);

        verify(routeRepository).getRouteById(newTicketDtoIn.getIdRoute());
        verify(routeRepository).updateRouteById(oldTicket.getRoute().getIdRoute(), updatedRoute);
        verify(ticketRepository).insertTicket(updatedTicket);
        verify(ticketMapper).toDtoOut(updatedTicket);
    }

    @Test
    void test_updateTicketById_ShouldUpdateSeatNrPriceRoutesSoldTickets_WhenNewTravelClassBus() {

        Location locationSrc = new Location("CHI", "Chisinau");
        Location locationDest = new Location("BAL", "Balti");

        Route route = new Route();
        route.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        route.setLocationSrc(locationSrc);
        route.setLocationDest(locationDest);
        route.setIdBus(1L);
        route.setSoldBussines(0L);
        route.setSoldEconomy(1L);
        Optional<Route> opRoute = Optional.ofNullable(route);

        Ticket oldTicket = new Ticket();
        oldTicket.setIdTicket(1);
        oldTicket.setRoute(route);
        oldTicket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        oldTicket.setClassType("ECO");
        oldTicket.setLuggageType("LRG");
        oldTicket.setSeatNr(1);
        oldTicket.setPrice(130d);

        TicketDtoIn newTicketDtoIn = new TicketDtoIn();
        newTicketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        newTicketDtoIn.setIdPassport("0023451200");
        newTicketDtoIn.setClassType("BUS");
        newTicketDtoIn.setLuggageType("SML");

        Ticket newTicket = new Ticket();
        newTicket.setIdTicket(null);
        newTicket.setRoute(route);
        newTicket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        newTicket.setClassType("BUS");
        newTicket.setLuggageType("SML");
        newTicket.setSeatNr(null);
        newTicket.setPrice(null);

        Ticket updatedTicket = new Ticket();
        updatedTicket.setIdTicket(1);
        updatedTicket.setRoute(route);
        updatedTicket.setPassenger(new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19)));
        updatedTicket.setClassType("BUS");
        updatedTicket.setLuggageType("SML");
        updatedTicket.setSeatNr(1);
        updatedTicket.setPrice(120d);

        Route updatedRoute = new Route();
        updatedRoute.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        updatedRoute.setLocationSrc(locationSrc);
        updatedRoute.setLocationDest(locationDest);
        updatedRoute.setIdBus(1L);
        updatedRoute.setSoldBussines(1L);
        updatedRoute.setSoldEconomy(0L);

        Bus bus = new Bus();
        bus.setIdBus(1);
        bus.setFactoryNR("MFnumberT125");
        bus.setTypeBus("MINIVAN");
        bus.setCapacityEconom((byte) 10);
        bus.setCapacityBusiness((byte) 10);
        bus.setTimeInspection(LocalDateTime.of(2019, 10, 12, 14, 0, 2));
        Optional<Bus> opBus = Optional.ofNullable(bus);

        when(ticketMapper.toEntity(newTicketDtoIn)).thenReturn(newTicket);
        when(ticketRepository.getTicketById(1)).thenReturn(Optional.of(oldTicket));
        when(routeRepository.getRouteById(oldTicket.getRoute().getIdRoute())).thenReturn(opRoute);
        when(busRepository.getBusById(oldTicket.getRoute().getIdBus().intValue())).thenReturn(opBus);
        when(ticketRepository.insertTicket(updatedTicket)).thenReturn(updatedTicket);

        ticketService.updateTicketById(1, newTicketDtoIn);

        verify(routeRepository).getRouteById(newTicketDtoIn.getIdRoute());
        verify(routeRepository).updateRouteById(oldTicket.getRoute().getIdRoute(), updatedRoute);
        verify(ticketRepository).insertTicket(updatedTicket);
        verify(ticketMapper).toDtoOut(updatedTicket);
    }

    @Test
    void test_updateTicketById__ShouldThrowException_WhenTicketBeforeUpdateNotFound() {

        TicketDtoIn newTicketDtoIn = new TicketDtoIn();
        newTicketDtoIn.setIdRoute("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10");
        newTicketDtoIn.setIdPassport("0023451200");
        newTicketDtoIn.setClassType("BUS");
        newTicketDtoIn.setLuggageType("SML");

        when(ticketMapper.toEntity(newTicketDtoIn)).thenReturn(new Ticket());
        when(ticketRepository.getTicketById(any())).thenReturn(Optional.empty());

        assertThatExceptionOfType(BadRequestException.class).
                isThrownBy(() -> ticketService.updateTicketById(1, newTicketDtoIn)).withMessage("404 Ticket with id: 1 NOT FOUND");
    }
}