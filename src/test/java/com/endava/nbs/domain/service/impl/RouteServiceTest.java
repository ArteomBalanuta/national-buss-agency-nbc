package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.RouteRepository;
import com.endava.nbs.domain.dto.RouteDto;
import com.endava.nbs.domain.entity.Location;
import com.endava.nbs.domain.entity.Route;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.domain.mapper.impl.RouteMapperImpl;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@Setter
@Getter
@ExtendWith(MockitoExtension.class)
public class RouteServiceTest {

    @InjectMocks
    RouteServiceImpl serviceRoute;

    @Mock
    RouteRepository routeRepositoryImplDAO;

    @Mock
    RouteMapperImpl routeMapper;

    @Test
    void getRouteShouldThrowResourceNotFound() throws SQLException {
        final String nonExistingRouteId = UUID.randomUUID().toString();

        when(routeRepositoryImplDAO.getRouteById(nonExistingRouteId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> serviceRoute.getRouteById(nonExistingRouteId)).isInstanceOf(BadRequestException.class);
        verify(routeRepositoryImplDAO).getRouteById(nonExistingRouteId);
    }

    @Test
    void getRouteByIdTest() throws SQLException {
        final String routeId = "6f2bdaec-ffc2-485e-b8b1-54e1566d5e10";

        final Route route = Route.builder()
                .idRoute(routeId)
                .locationDest(new Location("BAL"))
                .locationSrc(new Location("BEN"))
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        final RouteDto routeDTO = RouteDto.builder()
                .idRoute(routeId)
                .locationDest("BAL")
                .locationSrc("BEN")
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        when(routeRepositoryImplDAO.getRouteById(any(String.class))).thenReturn(Optional.of(route));
        when(routeMapper.toDto(any(Route.class))).thenReturn(routeDTO);

        assertThat(serviceRoute.getRouteById("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10")).isEqualTo(routeDTO);
    }

    @Test
    void deleteRouteById() throws Exception {
        final String routeId = "6f2bdaec-ffc2-485e-b8b1-54e1566d5e10";
        Route route = Route.builder().idRoute(routeId).build();

        when(routeRepositoryImplDAO.getRouteById(routeId)).thenReturn(Optional.of(route));
        serviceRoute.deleteRouteById(routeId);
        verify(routeRepositoryImplDAO).deleteRouteById(routeId);
        verify(routeRepositoryImplDAO).getRouteById(routeId);
    }

    @Test
    void deleteMissingRouteShouldThrowResourceNotFound() throws SQLException {
        final String nonExistingRouteId = UUID.randomUUID().toString();
        when(routeRepositoryImplDAO.getRouteById(nonExistingRouteId)).thenReturn(Optional.empty());
        assertThatThrownBy(() -> serviceRoute.deleteRouteById(nonExistingRouteId)).isInstanceOf(BadRequestException.class);
    }

    @Test
    void insertRoute() throws SQLException {
        final Route route = Route.builder()
                .idRoute(UUID.randomUUID().toString())
                .locationDest(new Location("BAL"))
                .locationSrc(new Location("CHIS"))
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        final RouteDto routeDTO = RouteDto.builder()
                .idRoute(route.getIdRoute())
                .locationDest("BAL")
                .locationSrc("CHIS")
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        String id = routeRepositoryImplDAO.insertRoute(route);

        lenient().when(routeMapper.toDto(route)).thenReturn(routeDTO);
        lenient().when(routeMapper.toEntity(routeDTO)).thenReturn(route);

        assertThat(serviceRoute.insertRoute(routeDTO).getIdRoute()).isEqualTo(id);
    }

    @Test
    void updateRouteById() throws SQLException {
        final String routeId = "6f2bdaec-ffc2-485e-b8b1-54e1566d5e10";

        final Route route = Route.builder()
                .idRoute(routeId)
                .locationDest(new Location("BAL"))
                .locationSrc(new Location("CHIS"))
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        final RouteDto routeDto = RouteDto.builder()
                .idRoute(routeId)
                .locationDest("BAL")
                .locationSrc("CHIS")
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        when(routeRepositoryImplDAO.getRouteById(routeId)).thenReturn(Optional.of(route));
        when(routeMapper.toDto(route)).thenReturn(routeDto);
        serviceRoute.updateRouteById(routeId, routeDto);
    }

    @Test
    void updateNonExistingShouldThrowResourceNotFound() throws SQLException {
        final String nonExistingRouteId = UUID.randomUUID().toString();

        final RouteDto routeDTO = RouteDto.builder()
                .idRoute(UUID.randomUUID().toString())
                .locationDest("BAL")
                .locationSrc("BEN")
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        when(routeRepositoryImplDAO.getRouteById(nonExistingRouteId)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> serviceRoute.updateRouteById(nonExistingRouteId, routeDTO)).isInstanceOf(BadRequestException.class);
        verify(routeRepositoryImplDAO).getRouteById(nonExistingRouteId);
    }

    @Test
    void getAllRoutes() throws SQLException {
        final Route routeF = Route.builder()
                .idRoute(UUID.randomUUID().toString())
                .locationDest(new Location("BAL"))
                .locationSrc(new Location("BEN"))
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        final Route routeS = Route.builder()
                .idRoute(UUID.randomUUID().toString())
                .locationDest(new Location("BAL"))
                .locationSrc(new Location("BAL"))
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        final RouteDto routeFDTO = RouteDto.builder()
                .idRoute(routeF.getIdRoute())
                .locationDest("BAL")
                .locationSrc("BEN")
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        final RouteDto routeSDTO = RouteDto.builder()
                .idRoute(routeS.getIdRoute())
                .locationDest("BAL")
                .locationSrc("BEN")
                .idBus(1L)
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        final List<Route> routeList = Arrays.asList(routeF, routeS);
        when(routeRepositoryImplDAO.getAllRoutes()).thenReturn(routeList);
        when(routeMapper.toDto(any(Route.class))).thenReturn(routeSDTO, routeFDTO);

        assertThat(serviceRoute.getAllRoutes()).containsOnly(routeSDTO, routeFDTO);

        verify(routeRepositoryImplDAO).getAllRoutes();
        verify(routeMapper, times(2)).toDto(any(Route.class));
    }

}