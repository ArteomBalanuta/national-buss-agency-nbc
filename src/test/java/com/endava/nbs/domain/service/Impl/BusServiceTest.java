package com.endava.nbs.domain.service.impl;

import com.endava.nbs.domain.dao.BusRepository;
import com.endava.nbs.domain.entity.Bus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BusServiceTest {

    @InjectMocks
    BusServiceImpl busService;

    @Mock
    BusRepository busRepository;

    @Test
    void getBusById() {
        busService.getBusById(111);
        verify(busRepository).findById(111);
    }

    @Test
    void deleteBusById() {
        Bus bus = new Bus();
        bus.setIdBus(100);
        bus.setFactoryNR("A1A2A3");
        bus.setCapacityEconom((byte) 20);
        bus.setCapacityBusiness((byte) 20);
        bus.setTimeInspection(LocalDateTime.of(2001,11,12,21,21,21));
        bus.setTypeBus("MINIVAN");
        Optional<Bus> opBus = Optional.of(bus);

        when(busRepository.getBusById(100)).thenReturn(opBus);
        busService.deleteBusById(100);
        verify(busRepository).deleteById(100);
    }

    @Test
    void insertBus() throws Exception {

        Bus bus = new Bus();
        bus.setIdBus(100);
        bus.setFactoryNR("A1A2A3");
        bus.setCapacityEconom((byte) 20);
        bus.setCapacityBusiness((byte) 20);
        bus.setTimeInspection(LocalDateTime.of(2001,11,12,21,21,21));
        bus.setTypeBus("MINIVAN");

        busService.insertBus(bus);
        verify(busRepository).saveAndFlush(bus);
    }

    @Test
    void updateBusById() throws Exception {
        Bus bus = new Bus();
        bus.setIdBus(100);
        bus.setFactoryNR("A1A2A3");
        bus.setCapacityEconom((byte) 20);
        bus.setCapacityBusiness((byte) 20);
        bus.setTimeInspection(LocalDateTime.of(2001,11,12,21,21,21));
        bus.setTypeBus("MINIVAN");
        Optional<Bus> optionalBus = Optional.of(bus);

        busService.updateBusById(100,bus);
        verify(busRepository).saveAndFlush(bus);
    }

    @Test
    void getAllBuses() throws Exception {
        busService.getAllBuses();
        verify(busRepository).findAll();
    }
}