package com.endava.nbs.domain.dao.impl;

import com.endava.nbs.config.DBUnitAbstractTest;
import com.endava.nbs.domain.dao.LocationRepository;
import com.endava.nbs.domain.dao.RouteRepository;
import com.endava.nbs.domain.entity.Route;
import lombok.Getter;
import lombok.Setter;
import org.dbunit.database.IDatabaseConnection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Setter
@Getter

@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class RouteRepositoryTest extends DBUnitAbstractTest {
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean;

    @Autowired
    RouteRepository routeRepositoryImplDAO;

    @Autowired
    LocationRepository locationRepositoryDAO;

    private IDatabaseConnection iDatabaseConnection = this.getConnection();

    public RouteRepositoryTest() throws Exception {
        super("Route Dao test");
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }


    @Test
    public void insertRoute() {
        Long routeCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM Routes", Long.class);
        assertNotNull(routeCount);

        Route routeF = Route.builder()
                .idBus(1L)
                .locationSrc(locationRepositoryDAO.getLocationById("CHI").get())
                .locationDest(locationRepositoryDAO.getLocationById("ORH").get())
                .timeDeparture(LocalDateTime.of(2018, 1, 1, 15, 0, 1, 00))
                .timeArrival(LocalDateTime.of(2021, 1, 2, 16, 0, 2, 00))
                .soldBussines(1L)
                .soldEconomy(1L)
                .build();

        routeRepositoryImplDAO.insertRoute(routeF);

        Long actualRouteCount = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM Routes", Long.class);
        assertNotNull(actualRouteCount);
        assertEquals(routeCount + 1, (long) actualRouteCount);
    }

    @Test
    public void updateRouteById() throws InterruptedException {
        Route insert = Route.builder()
                .idRoute(UUID.randomUUID().toString())
                .idBus(2L)
                .locationSrc(locationRepositoryDAO.getLocationById("ORH").get())
                .locationDest(locationRepositoryDAO.getLocationById("CHI").get())
                .timeDeparture(LocalDateTime.of(2018, 1, 1, 15, 0, 1, 00))
                .timeArrival(LocalDateTime.of(2021, 1, 2, 16, 0, 2, 00))
                .soldBussines(3L)
                .soldEconomy(3L)
                .build();

        Route update = Route.builder()
                .idRoute(UUID.randomUUID().toString())
                .idBus(1L)
                .locationSrc(locationRepositoryDAO.getLocationById("CHI").get())
                .locationDest(locationRepositoryDAO.getLocationById("BAL").get())
                .timeDeparture(LocalDateTime.of(2018, 1, 1, 15, 0, 1, 00))
                .timeArrival(LocalDateTime.of(2021, 1, 2, 16, 0, 2, 00))
                .soldBussines(2L)
                .soldEconomy(2L)
                .build();

        jdbcTemplate.update(
                "INSERT INTO " +
                        "Routes(idRoute,srcLocation,destLocation,depTime,arrivalTime,idBus,soldBusiness,soldEconomy) " +
                        "VALUES (?,?,?,TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),?,?,?)"
                , insert.getIdRoute()
                , insert.getLocationSrc().getIdLocation()
                , insert.getLocationDest().getIdLocation()
                , LocalDateTime.parse(insert.getTimeDeparture().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")).toString().replace('T', ' ')
                , LocalDateTime.parse(insert.getTimeArrival().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")).toString().replace('T', ' ')
                , insert.getIdBus()
                , insert.getSoldBussines()
                , insert.getSoldEconomy()
        );
        routeRepositoryImplDAO.updateRouteById(insert.getIdRoute(), update);

        Route updatedRoute = jdbcTemplate.queryForObject("SELECT * FROM Routes WHERE idRoute = " + "'" + insert.getIdRoute() + "'", new Object[]{}, (rs, rowNum) ->
                Route.builder()
                        .idRoute(rs.getString("idRoute"))
                        .locationSrc(locationRepositoryDAO.getLocationById(rs.getString("srcLocation")).get())
                        .locationDest(locationRepositoryDAO.getLocationById(rs.getString("destLocation")).get())
                        .timeDeparture(LocalDateTime.parse(rs.getString("depTime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .timeArrival(LocalDateTime.parse(rs.getString("arrivalTime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .idBus(rs.getLong("idBus"))
                        .soldBussines(rs.getLong("soldBusiness"))
                        .soldEconomy(rs.getLong("soldEconomy"))
                        .build());

        assertNotNull(updatedRoute);
        assertTrue(
                updatedRoute.getIdBus().equals(update.getIdBus())
                        && updatedRoute.getLocationSrc().equals(update.getLocationSrc())
                        && updatedRoute.getLocationDest().equals(update.getLocationDest())
                        && updatedRoute.getSoldBussines().equals(update.getSoldBussines())
                        && updatedRoute.getSoldEconomy().equals(update.getSoldEconomy())
        );
    }

    @Test
    public void deleteRouteById() throws Exception {
        Route insert = Route.builder()
                .idRoute(UUID.randomUUID().toString())
                .idBus(2L)
                .locationSrc(locationRepositoryDAO.getLocationById("BAL").get())
                .locationDest(locationRepositoryDAO.getLocationById("ORH").get())
                .timeDeparture(LocalDateTime.of(2018, 1, 1, 15, 0, 1, 00))
                .timeArrival(LocalDateTime.of(2021, 1, 2, 16, 0, 2, 00))
                .soldBussines(3L)
                .soldEconomy(3L)
                .build();

        jdbcTemplate.update(
                "INSERT INTO " +
                        "Routes(idRoute,srcLocation,destLocation,depTime,arrivalTime,idBus,soldBusiness,soldEconomy) " +
                        "VALUES (?,?,?,TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),TO_DATE(?,'YYYY-MM-DD hh24-mi-ss'),?,?,?)"
                , insert.getIdRoute()
                , insert.getLocationSrc().getIdLocation()
                , insert.getLocationDest().getIdLocation()
                , LocalDateTime.parse(insert.getTimeDeparture().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")).toString().replace('T', ' ')
                , LocalDateTime.parse(insert.getTimeArrival().toString(), DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")).toString().replace('T', ' ')
                , insert.getIdBus()
                , insert.getSoldBussines()
                , insert.getSoldEconomy()

        );
        routeRepositoryImplDAO.deleteRouteById(insert.getIdRoute());

        Long shouldBeZero = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM Routes WHERE idRoute = " + "'" + insert.getIdRoute() + "'", Long.class);
        assertNotNull(shouldBeZero);
        assertEquals(shouldBeZero.intValue(), 0);
    }

    @Test
    public void getRouteById() throws SQLException {
        Route selectedRoute = jdbcTemplate.queryForObject("SELECT * FROM Routes WHERE idRoute = '085b874b-f518-455f-b136-e9e269219a35' ", new Object[]{}, (rs, rowNum) ->
                Route.builder()
                        .idRoute(rs.getString("idRoute"))
                        .locationSrc(locationRepositoryDAO.getLocationById(rs.getString("srcLocation")).get())
                        .locationDest(locationRepositoryDAO.getLocationById(rs.getString("destLocation")).get())
                        .timeDeparture(LocalDateTime.parse(rs.getString("depTime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .timeArrival(LocalDateTime.parse(rs.getString("arrivalTime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .idBus(rs.getLong("idBus"))
                        .soldBussines(rs.getLong("soldBusiness"))
                        .soldEconomy(rs.getLong("soldEconomy"))
                        .build());
        assertEquals(routeRepositoryImplDAO.getRouteById("085b874b-f518-455f-b136-e9e269219a35").get().getIdRoute(), Objects.requireNonNull(selectedRoute).getIdRoute());
    }


    @Test
    public void getAll() throws SQLException {
        List<Route> routes = routeRepositoryImplDAO.getAllRoutes();

        List<Route> routesJDBC = jdbcTemplate.query("SELECT * FROM Routes ", new Object[]{}, (rs, rowNum) ->
                Route.builder()
                        .idRoute(rs.getString("idRoute"))
                        .locationSrc(locationRepositoryDAO.getLocationById(rs.getString("srcLocation")).get())
                        .locationDest(locationRepositoryDAO.getLocationById(rs.getString("destLocation")).get())
                        .timeDeparture(LocalDateTime.parse(rs.getString("depTime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .timeArrival(LocalDateTime.parse(rs.getString("arrivalTime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")))
                        .idBus(rs.getLong("idBus"))
                        .soldBussines(rs.getLong("soldBusiness"))
                        .soldEconomy(rs.getLong("soldEconomy"))
                        .build());

        assertTrue(routesJDBC.stream().map(Route::getIdRoute)
                .collect(Collectors.toList())
                .containsAll(routes.stream().map(Route::getIdRoute)
                        .collect(Collectors.toList())));
    }
}
