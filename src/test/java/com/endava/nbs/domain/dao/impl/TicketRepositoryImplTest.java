package com.endava.nbs.domain.dao.impl;

import com.endava.nbs.config.DBUnitAbstractTest;
import com.endava.nbs.domain.dao.TicketRepository;
import com.endava.nbs.domain.entity.Location;
import com.endava.nbs.domain.entity.Passenger;
import com.endava.nbs.domain.entity.Route;
import com.endava.nbs.domain.entity.Ticket;
import com.endava.nbs.domain.exception.BadRequestException;
import com.endava.nbs.rowMapper.TicketRowMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Import(TicketRowMapper.class)
public class TicketRepositoryImplTest extends DBUnitAbstractTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    TicketRowMapper ticketRowMapper;

    @Autowired
    TicketRepository ticketRepository;

    private final Passenger passenger;
    private final Route route;

    public TicketRepositoryImplTest() {

        super("TicketRepositoryImplTestCase");

        passenger = new Passenger("0023451200", "Andrei", "Cusnir", LocalDate.of(1998, 10, 19));

        route = new Route("6f2bdaec-ffc2-485e-b8b1-54e1566d5e10", new Location("CHI", "Chisinau"), new Location("BAL", "Balti"),
                LocalDateTime.now(), LocalDateTime.now().plusHours(1), 1L, 0L, 0L);
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testInsertTicket() {
        Ticket ticket = new Ticket();
        ticket.setRoute(route);
        ticket.setPassenger(passenger);
        ticket.setClassType("BUS");
        ticket.setLuggageType("LRG");
        ticket.setSeatNr(2);
        ticket.setPrice(150d);

        Integer beforeInsertLocationCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM TICKETS", Integer.class);

        ticketRepository.insertTicket(ticket);

        Integer expectedTicketCount = beforeInsertLocationCount + 1;
        Integer actualTicketCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM TICKETS", Integer.class);

        assertEquals(expectedTicketCount, actualTicketCount);
    }

    @Test
    public void testDeleteTicketById() {
        Integer beforeDeleteTicketCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM TICKETS", Integer.class);

        ticketRepository.deleteTicketById(2);
        ticketRepository.deleteTicketById(3);

        Integer expectedTicketCount = beforeDeleteTicketCount - 2;
        Integer actualTicketCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM TICKETS", Integer.class);

        assertEquals(expectedTicketCount, actualTicketCount);
    }

    @Test
    public void testGetTicketById() {
        Ticket expectedTicket = ticketRepository.getTicketById(3).orElseThrow(
                () -> new BadRequestException(HttpStatus.NOT_FOUND, "Error on testGetTicketById: NO TICKET WITH SUCH ID"));

        Ticket actualTicket = jdbcTemplate.queryForObject("SELECT * FROM TICKETS WHERE idTicket = 3", ticketRowMapper);


        assertEquals(expectedTicket, actualTicket);
    }

    @Test
    public void getAllTickets() {
        List<Ticket> expectedTickets = jdbcTemplate.query("SELECT * FROM TICKETS", ticketRowMapper);
        List<Ticket> actualTickets = ticketRepository.getAllTickets();

        assertEquals(expectedTickets, actualTickets);
    }
}