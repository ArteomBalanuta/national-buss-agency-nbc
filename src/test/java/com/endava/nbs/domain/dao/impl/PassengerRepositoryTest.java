package com.endava.nbs.domain.dao.impl;

import com.endava.nbs.config.Config;
import com.endava.nbs.config.TestDataSourceConfig;
import com.endava.nbs.domain.dao.PassengerRepository;
import com.endava.nbs.domain.entity.Passenger;
import com.endava.nbs.rowMapper.PassengerRowMapper;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.spring.api.DBRider;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ContextConfiguration(classes = {Config.class, TestDataSourceConfig.class})
@DataJpaTest
@DBRider
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataSet(value = "PassengersTestDataSet.xml", cleanAfter = true)
public class PassengerRepositoryTest {

    private static final String PASSENGER_ID_ANDREI = "0023451200";
    private static final String PASSENGER_ID_RADU = "0023451209";
    private static final String SELECT_COUNT_FROM_PASSENGERS = "SELECT COUNT (*) FROM PASSENGERS";
    private static final String SELECT_FIRST_NAME_FROM_PASSENGERS_WHERE_PASSPORT_ID = "SELECT firstName FROM PASSENGERS WHERE idPassport = '%s'";

    @Autowired(required = false)
    private JdbcTemplate jdbcTemplate;

    @Autowired
    PassengerRepository passengerRepository;

    @Test
    @DisplayName("")
    public void testInsertPassenger() {
        Integer beforeInsertPassengerCount = jdbcTemplate.queryForObject(SELECT_COUNT_FROM_PASSENGERS, Integer.class);
        Passenger andrei = new Passenger();
        andrei.setIdPassport("1347097162");
        andrei.setFirstName("Andrei");
        andrei.setLastName("Ivanov");
        andrei.setBirthDate(LocalDate.of(1982, 3, 23));
        passengerRepository.saveAndFlush(andrei);
        Passenger valeriu = new Passenger();
        valeriu.setIdPassport("001234091748");
        valeriu.setFirstName("Valeriu");
        valeriu.setLastName("Turcan");
        valeriu.setBirthDate(LocalDate.of(1986, 6, 19));
        passengerRepository.saveAndFlush(valeriu);
        Integer expectedPassengerCount = beforeInsertPassengerCount + 2;
        Integer actualPassengerCount = jdbcTemplate.queryForObject(SELECT_COUNT_FROM_PASSENGERS, Integer.class);
        assertEquals(expectedPassengerCount, actualPassengerCount);
    }

    @Test
    public void testUpdatedPassengerById() {
        Passenger petru = new Passenger();
        petru.setIdPassport("0032184200");
        petru.setFirstName("Petru");
        petru.setLastName("Cusnir");
        petru.setBirthDate(LocalDate.of(1998, 10, 19));
        String id = "0032184200";
        passengerRepository.saveAndFlush(petru);
        String actualPassengerName = jdbcTemplate.queryForObject(String.format(SELECT_FIRST_NAME_FROM_PASSENGERS_WHERE_PASSPORT_ID, id), String.class);
        assertEquals("Petru", actualPassengerName);
    }

    @Test
    public void testDeletePassengerById() {
        Integer beforeDeletePassengerCount = jdbcTemplate.queryForObject(SELECT_COUNT_FROM_PASSENGERS, Integer.class);
        passengerRepository.deleteById(PASSENGER_ID_ANDREI);
        passengerRepository.deleteById(PASSENGER_ID_RADU);
        passengerRepository.flush();
        Integer expectedPassengerCount = beforeDeletePassengerCount - 2;
        Integer actualPassengerCount = jdbcTemplate.queryForObject(SELECT_COUNT_FROM_PASSENGERS, Integer.class);
        assertEquals(expectedPassengerCount, actualPassengerCount);
    }

    @Test
    public void testGetPassengerById() {
        Passenger newPassenger = new Passenger();
        newPassenger.setFirstName("Andrei");
        String expectedPassengerName = passengerRepository.findById(PASSENGER_ID_ANDREI).get().getFirstName();
        String actualFirstName = jdbcTemplate.queryForObject(
                String.format(SELECT_FIRST_NAME_FROM_PASSENGERS_WHERE_PASSPORT_ID, PASSENGER_ID_ANDREI), String.class);
        assertEquals(expectedPassengerName, actualFirstName);
    }

    @Test
    public void testGetAllPassengers() {
        List<Passenger> passengers = jdbcTemplate.query("SELECT * FROM PASSENGERS", new PassengerRowMapper());
        List<Passenger> passengers1 = passengerRepository.findAll();
        assertEquals(passengers, passengers1);
    }
}
