package com.endava.nbs.domain.dao.impl;

import com.endava.nbs.config.DBUnitAbstractTest;
import com.endava.nbs.domain.dao.LocationRepository;
import com.endava.nbs.domain.entity.Location;
import com.endava.nbs.rowMapper.LocationRowMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@DataJpaTest
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class LocationRepositoryImplTest extends DBUnitAbstractTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    LocationRepository locationRepository;

    public LocationRepositoryImplTest() {
        super("RepositoryLocationTestCase");
    }


    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void testInsertLocation() {
        Location location1 = new Location();
        Location location2 = new Location();
        location1.setIdLocation("TIR");
        location1.setCityName("Tiraspol");
        location2.setIdLocation("OCN");
        location2.setCityName("Ocnita");

        Integer beforeInsertLocationCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM LOCATIONS", Integer.class);

        locationRepository.insertLocation(location1);
        locationRepository.insertLocation(location2);

        Integer expectedLocationCount = beforeInsertLocationCount + 2;
        Integer actualLocationCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM LOCATIONS", Integer.class);

        assertEquals(expectedLocationCount, actualLocationCount);
    }

    @Test
    public void testDeleteLocationById() {

        Integer beforeDeleteLocationCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM LOCATIONS", Integer.class);

        locationRepository.deleteLocationById("BEN");
        locationRepository.deleteLocationById("COM");

        Integer expectedLocationCount = beforeDeleteLocationCount - 2;
        Integer actualLocationCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM LOCATIONS", Integer.class);

        assertEquals(expectedLocationCount, actualLocationCount);
    }

    @Test
    public void testGetLocationById() {
        Location newLocation = new Location();
        newLocation.setCityName("Tighina");

        String expectedCityName = locationRepository.getLocationById("CAH").get().getCityName();
        String actualCityName = jdbcTemplate.queryForObject("SELECT cityName FROM LOCATIONS WHERE idLocation = 'CAH'", String.class);

        assertEquals(expectedCityName, actualCityName);
    }


    @Test
    public void testGetAllLocations() {

        List<Location> expectedLocations = jdbcTemplate.query("SELECT * FROM LOCATIONS", new LocationRowMapper());
        List<Location> actualLocations = locationRepository.getAllLocations();

        assertEquals(expectedLocations, actualLocations);
    }
}