package com.endava.nbs.domain.dao.impl;

import com.endava.nbs.config.DBUnitAbstractTest;
import com.endava.nbs.domain.dao.BusRepository;
import com.endava.nbs.domain.entity.Bus;
import com.endava.nbs.rowMapper.BusRowMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class BusRepositoryTest extends DBUnitAbstractTest {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    BusRepository busRepository;

    public BusRepositoryTest() {
        super("RepositoryBusTestCase");
    }

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void insertBus() throws Exception {
        Bus bus1 = new Bus();
        Bus bus2 = new Bus();
        bus1.setIdBus(1);
        bus1.setFactoryNR("test1");
        bus1.setTypeBus("MINIVAN");
        bus1.setTimeInspection(LocalDateTime.of(2001,11,12,21,21,21));
        bus2.setIdBus(2);
        bus2.setFactoryNR("test2");
        bus2.setTypeBus("MINIVAN");
        bus2.setTimeInspection(LocalDateTime.of(2001,11,12,21,21,21));


        Integer beforeInsertBusCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM BUSES", Integer.class);
        busRepository.saveAndFlush(bus1);
        busRepository.saveAndFlush(bus2);
        Integer expectedBusCount = beforeInsertBusCount + 2;
        Integer actualBusCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM BUSES", Integer.class);

        assertEquals(expectedBusCount, actualBusCount);
    }

    @Test
    public void deleteBusById() throws SQLException {
        Integer beforeDeleteBusCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM BUSES", Integer.class);

        busRepository.deleteById(4);
        busRepository.deleteById(5);

        Integer expectedBusCount = beforeDeleteBusCount - 2;
        Integer actualBusCount = jdbcTemplate.queryForObject("SELECT COUNT (*) FROM BUSES", Integer.class);

        assertEquals(expectedBusCount, actualBusCount);
    }

    @Test
    public void getBusById() throws SQLException {

        Optional<Bus> actualId = busRepository.findById(1);
        String expectedBusFactoryNR = jdbcTemplate.queryForObject("SELECT factoryNR FROM BUSES WHERE idBus = 1 ", String.class);

        assertEquals(expectedBusFactoryNR, actualId.get().getFactoryNR());
    }

    @Test
    public void updateBusById() throws SQLException {
        Bus newBus = new Bus();
        newBus.setIdBus(1);
        newBus.setFactoryNR("Test1A23");
        newBus.setTypeBus("MINIVAN");
        newBus.setCapacityEconom((byte) 10);
        newBus.setCapacityBusiness((byte) 11);
        newBus.setTimeInspection(LocalDateTime.of(2001,11,12,21,21,21));
        String actualBusFactoryNR = jdbcTemplate.queryForObject("SELECT factoryNR FROM BUSES WHERE idBus = 1 ", String.class);


        assertEquals("Test1A23", actualBusFactoryNR);
    }

    @Test
    public void getAllBuses() throws Exception {
        List<Bus> expectedBuses = jdbcTemplate.query("SELECT * FROM BUSES", new BusRowMapper());
        List<Bus> actualBuses = busRepository.findAll();
        assertEquals(expectedBuses, actualBuses);
    }
}